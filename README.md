# 7 Confirmations

### Run in dev mode

- Run `npm i`
- Create files `.env`, `.env.build`, `.env.local`
- `.env` and `.env.local` files should contain below configurations

```
API_VERSION=v1
PORT=8080
BASE_URL=http://localhost:8080
MONGODB_URI=
NODEMAILER_EMAIL=
NODEMAILER_EMAIL_PASS=
AUTHENTICATED_USER_PROPERTY_NAME=
BCRYPT_SALT_WORK_FACTOR=
ACCESS_TOKEN_SECRET=
REFRESH_TOKEN_SECRET=
PAYTM_MID=
PAYTM_WEBSITE=
PAYTM_CHANNEL_ID=
PAYTM_INDUSTRY_TYPE_ID=
PAYTM_MERCHANT_KEY=
```

### Seed Product
```
{
    "_id" : ObjectId("6240a96cb08713b405e30073"),
    "heading" : "Product 1",
    "subHeading" : "Product Sub Heading",
    "productImage" : "batman-immunity.png",
    "productPrice" : "120",
    "infoList" : [ 
        {
            "id" : "1",
            "value" : "Info 1"
        }, 
        {
            "id" : "2",
            "value" : "Info 2"
        }
    ]
}
,
    "_id" : ObjectId("6240a96cb08713b405e30074"),
    "heading" : "Product 2",
    "subHeading" : "Product Sub Heading",
    "productImage" : "blossom-pcos-juice.png",
    "productPrice" : "160",
    "infoList" : [ 
        {
            "id" : "1",
            "value" : "Info 1"
        }, 
        {
            "id" : "2",
            "value" : "Info 2"
        }
    ]
}
```


- Run `npm run dev` to run in development mode on port 8080



### Deploy to Google App Engine

- Create `app.yaml` file and add below config

```
runtime: nodejs
env: flex
automatic_scaling:
  min_num_instances: 1
  max_num_instances: 10
  cool_down_period_sec: 180
  cpu_utilization:
    target_utilization: 0.9
resources:
  cpu: 2
  memory_gb: 4.6
  disk_size_gb: 10
network:
  name: default
env_variables:
  API_VERSION:
  PORT:
  BASE_URL:
  MONGODB_URI:
  NODEMAILER_EMAIL:
  NODEMAILER_EMAIL_PASS:
  AUTHENTICATED_USER_PROPERTY_NAME:
  BCRYPT_SALT_WORK_FACTOR:
  ACCESS_TOKEN_SECRET:
  REFRESH_TOKEN_SECRET:
  PAYTM_MID:
  PAYTM_WEBSITE:
  PAYTM_CHANNEL_ID:
  PAYTM_INDUSTRY_TYPE_ID:
  PAYTM_MERCHANT_KEY:
```

- Delete `.next` folder

- Change the version in package.json file

- To deploy, run `npm run deploy`
