import axios from "../utils/client/axiosInstance.util";
import { getUserToken } from "../utils/client/auth.util";

export async function getAllProducts() {
  let token = getUserToken();

  let { data } = await axios.get(`/products`, {
    headers: {
      Authorization: `Bearer ${getUserToken()}`,
      "x-auth-token-compulsory": false,
    },
  });

  if (!token) {
    let cart = localStorage.getItem("cart");
    if (!cart) {
      localStorage.setItem("cart", "[]");
    }

    cart = JSON.parse(localStorage.getItem("cart"));

    data = data.map((v) => {
      if (cart.includes(v._id)) v.addedToCart = true;
      return v;
    });
  }

  console.log("get all products res data:::", data);

  return data;
}

export async function getProductGroupType(productGroup, productType) {
  let token = getUserToken();

  let { data } = await axios.get(
    `/products/group/${productGroup}/type/${productType}`,
    {
      headers: {
        Authorization: `Bearer ${getUserToken()}`,
        "x-auth-token-compulsory": false,
      },
    }
  );

  if (!token) {
    let cart = localStorage.getItem("cart");
    if (!cart) {
      localStorage.setItem("cart", "[]");
    }

    cart = JSON.parse(localStorage.getItem("cart"));

    if (cart.includes(data._id)) data.addedToCart = true;
  }

  console.log("get product by group res data:::", data);

  return data;
}

export async function addProductToCart(products) {
  if (typeof products === "string") products = [products];

  let token = getUserToken();

  console.log("token ===", token);

  if (!products) {
    let cart = localStorage.getItem("cart");
    if (cart) products = JSON.parse(cart);
  }

  let cart = localStorage.getItem("cart");
  if (!cart) {
    localStorage.setItem("cart", "[]");
  }

  cart = JSON.parse(localStorage.getItem("cart"));

  products.forEach((productId) => cart.push(productId));

  localStorage.setItem("cart", JSON.stringify(cart));

  if (!token) {
    return cart.length;
  }

  let { data } = await axios.post(
    `/products/cart/add`,
    { products },
    {
      headers: {
        Authorization: `Bearer ${token}`,
        //   "x-token-required": false,
      },
    }
  );

  console.log("add product to cart res data:::", data);

  return data;
}

export async function removeProductFromCart(productId) {
  let token = getUserToken();

  let cart = localStorage.getItem("cart");
  if (!cart) {
    return 0;
  }

  cart = JSON.parse(localStorage.getItem("cart"));

  // cart.push(productId);

  let idx = cart.findIndex((v) => v === productId);

  if (idx > -1) {
    cart.splice(idx, 1);
    localStorage.setItem("cart", JSON.stringify(cart));
  }

  if (!token) {
    return cart.length;
  }

  let { data } = await axios.post(
    `/products/${productId}/cart/remove`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  console.log("remove product from cart res data:::", data);

  return data;
}

export async function getProductsInCart() {
  let token = getUserToken();

  console.log("token = ", token);

  if (!token) {
    let cart = localStorage.getItem("cart");

    console.log("cart = ", cart);

    if (!cart) return;

    cart = JSON.parse(localStorage.getItem("cart"));

    console.log("cart = ", cart);

    let { data } = await axios.post(`/products/listed`, { productIds: cart });

    console.log("data products listed = ", data);

    return { products: data };
  }

  let { data } = await axios.get(`/products/cart`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  console.log("get product in cart res data:::", data);

  return data;
}

export default {
  getAllProducts,
  getProductGroupType,
  addProductToCart,
  removeProductFromCart,
  getProductsInCart,
};
