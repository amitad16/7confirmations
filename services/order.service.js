import Router from "next/router";

import axios from "../utils/client/axiosInstance.util";
import { getUserToken } from "../utils/client/auth.util";

export async function handleProcessOrder(orderDetails) {
  try {
    let { data } = await axios.post(`/order/new`, orderDetails, {
      headers: {
        Authorization: `Bearer ${getUserToken()}`,
      },
    });

    console.log("order placed res data:::", data);

    document.write(data);

    return data;
  } catch (err) {
    console.log("err = ", err.response.data);

    if (
      err.response &&
      err.response.data &&
      err.response.data.statusCode === 422
    ) {
      return { error: err.response.data.message };
    } else if (
      err.response &&
      err.response.data &&
      err.response.data.statusCode
    ) {
      Router.push("/login");
    }
  }
}

export async function getOrderStatus(orderId) {
  let { data } = await axios.get(`/order/${orderId}/status`, {
    headers: {
      Authorization: `Bearer ${getUserToken()}`,
    },
  });

  console.log("order placed res data:::", data);

  return data;
}

export default { handleProcessOrder, getOrderStatus };
