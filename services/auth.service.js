import axios from "../utils/client/axiosInstance.util";
import { addProductToCart } from "./product.service";
// import { getUserToken } from "../utils/client/auth.util";

async function login(userData) {
  let { data } = await axios.post(`/auth/login`, userData);

  console.log("login res data:::", data);

  localStorage.setItem(
    "__user__",
    JSON.stringify({ accessToken: data.accessToken, user: data.user })
  );

  addProductToCart();

  return { success: true, accessToken: data.accessToken, user: data.user };
}

async function register(userData) {
  let { data } = await axios.post(`/auth/register`, userData);

  console.log("register res data:::", data);

  localStorage.setItem(
    "__user__",
    JSON.stringify({ accessToken: data.accessToken, user: data.user })
  );

  addProductToCart();

  return { success: true, accessToken: data.accessToken, user: data.user };
}

export default { login, register };
