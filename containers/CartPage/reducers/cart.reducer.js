import {
  GET_CART_PRODUCTS,
  REMOVE_FROM_CART_PAGE,
  ADD_TO_CART_PAGE,
} from "../actions/types";
import {
  updateObject,
  updateItemInArray,
  removeItemByIndexInArray,
} from "../../../utils/client/object.util";

const initialState = {
  items: [],
  processing: {
    addProductToCart: false,
    removeProductFromCart: false,
    getCartProducts: false,
  },
  error: {
    addProductToCart: "",
    removeProductFromCart: "",
    getCartProducts: "",
  },
};

function getCartProductsRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getCartProducts: true },
    error: { ...state.error, getCartProducts: "" },
  });
}

function getCartProductsSucces(state, action) {
  return updateObject(state, action.payload);
}

function getCartProductsFaliure(state, action) {
  return updateObject(state, {
    error: { ...state.error, getCartProducts: action.payload.error },
  });
}

function getCartProductsFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getCartProducts: false },
  });
}

// Add to cart
function addToCartRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, addProductToCart: true },
    error: { ...state.error, addProductToCart: "" },
  });
}

function addToCartSucces(state, action) {
  return updateObject(state, {
    items: [...state.items, ...action.payload.producs],
  });
}

function addToCartFaliure(state, action) {
  // return updateObject(state, {
  //   error: { ...state.error, addProductToCart: action.payload.error },
  // });
  return state;
}

function addToCartFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, addProductToCart: false },
  });
}

// Remove from cart
function removeFromCartRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, removeProductFromCart: true },
    error: { ...state.error, removeProductFromCart: "" },
  });
}

function removeFromCartSucces(state, action) {
  return updateObject(state, {
    items: removeItemByIndexInArray(
      state.items,
      "_id",
      action.payload.productId,
      1
    ),
  });
}

function removeFromCartFaliure(state, action) {
  return updateObject(state, {
    error: { ...state.error, removeProductFromCart: action.payload.error },
  });
}

function removeFromCartFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, removeProductFromCart: false },
  });
}

export default (state = initialState, action) => {
  let { type } = action;

  switch (type) {
    case GET_CART_PRODUCTS.REQUEST:
      return getCartProductsRequest(state, action);

    case GET_CART_PRODUCTS.SUCCESS:
      return getCartProductsSucces(state, action);

    case GET_CART_PRODUCTS.FAILURE:
      return getCartProductsFaliure(state, action);

    case GET_CART_PRODUCTS.FULFIL:
      return getCartProductsFulfil(state, action);

    // add to cart
    case ADD_TO_CART_PAGE.REQUEST:
      return addToCartRequest(state, action);

    case ADD_TO_CART_PAGE.SUCCESS:
      return addToCartSucces(state, action);

    case ADD_TO_CART_PAGE.FAILURE:
      return addToCartFaliure(state, action);

    case ADD_TO_CART_PAGE.FULFIL:
      return addToCartFulfil(state, action);

    // remove from cart
    case REMOVE_FROM_CART_PAGE.REQUEST:
      return removeFromCartRequest(state, action);

    case REMOVE_FROM_CART_PAGE.SUCCESS:
      return removeFromCartSucces(state, action);

    case REMOVE_FROM_CART_PAGE.FAILURE:
      return removeFromCartFaliure(state, action);

    case REMOVE_FROM_CART_PAGE.FULFIL:
      return removeFromCartFulfil(state, action);

    default:
      return state;
  }
};
