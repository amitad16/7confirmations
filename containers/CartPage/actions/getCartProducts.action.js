// Action Type
import { GET_CART_PRODUCTS } from "./types";

// Service
import { getProductsInCart } from "../../../services/product.service";

export default () => async (dispatch) => {
  try {
    console.log("getting all products");

    dispatch({ type: GET_CART_PRODUCTS.REQUEST });

    let data = await getProductsInCart();

    console.log("action => get all products", data);

    dispatch({
      type: GET_CART_PRODUCTS.SUCCESS,
      payload: { items: data.products },
    });
  } catch (err) {
    console.log("cannot get all products", err);
    dispatch({
      type: GET_CART_PRODUCTS.FAILURE,
      payload: { error: "Cannot remove item" },
    });
  } finally {
    dispatch({ type: GET_CART_PRODUCTS.FULFIL });
  }
};
