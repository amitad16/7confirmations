// Action Type
import { REMOVE_FROM_CART_PAGE } from "./types";

// Service
import { removeProductFromCart } from "../../../services/product.service";

export default (productId) => async (dispatch) => {
  try {
    dispatch({ type: REMOVE_FROM_CART_PAGE.REQUEST });

    let data = await removeProductFromCart(productId);

    console.log("action => get all products", data);

    dispatch({ type: REMOVE_FROM_CART_PAGE.SUCCESS, payload: { productId } });
  } catch (err) {
    console.log("cannot get all products", err);
    dispatch({ type: REMOVE_FROM_CART_PAGE.FAILURE });
  } finally {
    dispatch({ type: REMOVE_FROM_CART_PAGE.FULFIL });
  }
};
