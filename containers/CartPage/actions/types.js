import createProtocol from "../../../utils/client/protocol.util";

export const ADD_TO_CART_PAGE = createProtocol("ADD_TO_CART_PAGE");

export const REMOVE_FROM_CART_PAGE = createProtocol("REMOVE_FROM_CART_PAGE");

export const GET_CART_PRODUCTS = createProtocol("GET_CART_PRODUCTS");

export const GET_LISTED_PRODUCTS = createProtocol("GET_LISTED_PRODUCTS");
