// Action Type
import { ADD_TO_CART_PAGE } from "./types";

// Service
import { addProductToCart } from "../../../services/product.service";

export default (products) => async (dispatch) => {
  try {
    dispatch({ type: ADD_TO_CART_PAGE.REQUEST });

    let data = await addProductToCart(products);

    console.log("action => get all products", data);

    dispatch({ type: ADD_TO_CART_PAGE.SUCCESS, payload: { products } });
  } catch (err) {
    console.log("cannot get all products", err);
    dispatch({ type: ADD_TO_CART_PAGE.FAILURE });
  } finally {
    dispatch({ type: ADD_TO_CART_PAGE.FULFIL });
  }
};
