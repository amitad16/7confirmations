import { useEffect, useState } from "react";
import { connect } from "react-redux";
import Link from "next/link";

import { Grid, Container, Message } from "semantic-ui-react";

import styles from "./CartPage.css";

// Actions
import { getCartProducts, removeProductFromCart } from "./actions";

import Navigation from "../../components/Navigation/Navigation";
import CartItems from "./CartItems";
import CheckoutBill from "../../components/CheckoutBill";
import { handleProcessOrder } from "../../services/order.service";
import AddressForm from "./AddressForm";

const CartPage = ({ cart, getCartProducts, removeProductFromCart }) => {
  const [windowDimensions, setWindowDimensions] = useState();
  const [productCols, setProductCols] = useState();
  const [subtotal, setSubtotal] = useState(0);
  const [shippingCost, setShippingCost] = useState(0);
  const [totalCost, setTotalCost] = useState(0);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [deliveryAddress, setDeliveryAddress] = useState({
    fullname: "",
    email: "",
    mobile: "",
    address: "",
    pin: "",
    state: "",
    country: "India",
  });

  const [formError, setFormError] = useState("");

  function handleChange(e, { name, value }) {
    setDeliveryAddress((prevState) => ({ ...prevState, [name]: value }));
  }

  useEffect(() => {
    getCartProducts();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);

    setIsLoggedIn(!!window.localStorage.getItem("__user__"));

    autofillForm();

    return () => {
      window.removeEventListener("resize", updateWindowDimensions);
    };
  }, []);

  function autofillForm() {
    if (window && window.localStorage) {
      let user = window.localStorage.getItem("__user__");

      if (!user) return;

      user = JSON.parse(user);

      if (!user) return;

      if (!user.user) return;

      let { fullname, email } = user.user;

      setDeliveryAddress((prevState) => ({ ...prevState, fullname, email }));
    }
  }

  useEffect(() => {
    let _subtotal = 0;
    let _shippingCost = 0;

    cart &&
      cart.items &&
      cart.items.forEach((item) => (_subtotal += parseFloat(item.productPrice)));

    setSubtotal(_subtotal);
    setShippingCost(_shippingCost);
  }, [cart]);

  useEffect(() => {
    setTotalCost(subtotal + shippingCost);
  }, [subtotal, shippingCost]);

  function updateWindowDimensions() {
    setWindowDimensions({
      width: window.innerWidth,
      height: window.innerHeight,
    });

    if (window.innerWidth > 1024) setProductCols(3);
    else if (window.innerWidth > 742) setProductCols(4);
    else if (window.innerWidth > 500) setProductCols(3);
    else setProductCols(2);
  }

  function handleRemoveProductFromCart(productId) {
    removeProductFromCart(productId);
  }

  async function handleCheckout() {
    if (!isLoggedIn) return setFormError("Please Login");

    if (!Object.values(deliveryAddress).every((v) => !!v))
      return setFormError("Invalid Delivery Details");

    setFormError("");

    let transactionAmount = totalCost;
    let channelId = "WEB";

    let data = await handleProcessOrder({
      deliveryAddress,
      products: cart.items.map((v) => v._id),
      transactionAmount,
      channelId,
    });

    console.log("DATA ===", data);

    if (data.error) setFormError(data.error);
  }

  return (
    <div className={styles.CartPage}>
      <Navigation />
      <div className={styles.Banner}>
        <div className={styles.Banner__heading}>Need some guidance?</div>&nbsp;
        <Link href="/survey/new">
          <a>Take the quiz</a>
        </Link>
      </div>
      <Container>
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column
              largeScreen={10}
              tablet={16}
              mobile={16}
              className={styles.CartItems__wrapper}
            >
              <CartItems
                cartItems={cart.items}
                handleRemoveProductFromCart={handleRemoveProductFromCart}
              />
            </Grid.Column>
            <Grid.Column largeScreen={6} tablet={16} mobile={16}>
              {isLoggedIn ? (
                <AddressForm
                  deliveryAddress={deliveryAddress}
                  formError={formError}
                  handleChange={handleChange}
                />
              ) : (
                <>
                  <div className={styles.CartPage__warning}>
                    Please{" "}
                    <Link href="/login?redirect=/cart">
                      <a>Login</a>
                    </Link>{" "}
                    before checking out
                  </div>
                  {formError && <Message error header={formError}></Message>}
                </>
              )}
              <CheckoutBill
                subtotal={subtotal}
                shipping={shippingCost}
                total={totalCost}
                handleCheckout={handleCheckout}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  cart: state.cart,
});

const mapDispatchToProps = { getCartProducts, removeProductFromCart };

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
