import styles from "./CartPage.css";
import Navigation from "../../components/Navigation/Navigation";

const Header = () => {
  return (
    <div className={`${styles.Header}`}>
      <div className={`ui page`}>
        <Navigation />
        <div className={styles.Header__content}>
          <p className={styles.Header__content__heading}>
            {" "}
            Healthy products made just for you
          </p>
        </div>
      </div>
    </div>
  );
};

export default Header;
