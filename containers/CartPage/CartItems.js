import styles from "./CartPage.css";
import Link from "next/link";

import { CartProductCard } from "../../components/Cards";
import { Grid, Icon } from "semantic-ui-react";

const CartItems = ({ cartItems, handleRemoveProductFromCart }) => {
  return (
    <div className={styles.CartItems}>
      {cartItems && cartItems.length ? (
        // <Grid.Row>
        cartItems.map((v) => (
          <Grid.Row key={v._id} className={styles.CartProductCard__wrapper}>
            <div
              className={styles.CartProductCard__remove}
              onClick={() => handleRemoveProductFromCart(v._id)}
            >
              <Icon name="close" color="grey" />
            </div>

            <CartProductCard {...v} addedToCart={true} />
          </Grid.Row>
        ))
      ) : (
        // </Grid.Row>
        <div className={styles.CartItems__empty}>
          <p>You have no items in your cart.</p>
          <Link href="/products">
            <a>Browse Products</a>
          </Link>
        </div>
      )}
    </div>
  );
};

export default CartItems;
