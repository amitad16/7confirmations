import { Form, Label, Grid, Message } from "semantic-ui-react";

import styles from "./CartPage.css";

const AddressForm = ({ deliveryAddress, formError, handleChange }) => {
  return (
    <Grid className={styles.AddressForm__wrapper}>
      <Grid.Row>
        <Grid.Column width={12} className={styles.AddressForm}>
          {!!formError && <Message error header={formError} />}

          <Form>
            <Form.Input
              label="Full Name"
              placeholder="Full Name"
              name="fullname"
              value={deliveryAddress.fullname}
              onChange={handleChange}
            />
            <Form.Input
              label="Email"
              placeholder="Email"
              name="email"
              value={deliveryAddress.email}
              onChange={handleChange}
            />
            <Form.Input
              label="Mobile"
              labelPosition="right"
              type="text"
              placeholder="8965231456"
              name="mobile"
              value={deliveryAddress.mobile}
              onChange={handleChange}
            >
              <Label basic>+91</Label>
              <input />
            </Form.Input>
            <Form.Input
              label="Address"
              placeholder="Address"
              name="address"
              value={deliveryAddress.address}
              onChange={handleChange}
            />
            <Form.Input
              label="PIN Code"
              placeholder="PIN Code"
              name="pin"
              width={6}
              value={deliveryAddress.pin}
              onChange={handleChange}
            />
            <Form.Input
              label="State"
              placeholder="State"
              name="state"
              value={deliveryAddress.state}
              onChange={handleChange}
            />
            <Form.Input
              label="Country"
              placeholder="Country"
              name="country"
              value={deliveryAddress.country}
              onChange={handleChange}
            />
          </Form>

          {!!formError && <Message error header={formError} />}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default AddressForm;
