import { useState } from "react";
import Router from "next/router";
import { connect } from "react-redux";

import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
} from "semantic-ui-react";
import Link from "next/link";

import { login } from "../actions";
import Navigation from "../../components/Navigation/Navigation";

const LoginPage = ({ auth, login }) => {
  const [formValues, setFormValues] = useState({
    email: "",
    password: "",
  });

  function handleChange(_, data) {
    setFormValues((prevState) => ({ ...prevState, [data.name]: data.value }));
  }

  function handleSubmit(e) {
    e.preventDefault();

    console.log("formValues = ", formValues);

    login(formValues, Router.router.query.redirect);
  }

  return (
    <div>
      <Navigation />
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" textAlign="center" style={{ color: "#F1574B" }}>
            Log-in to your account
          </Header>
          {auth.error.status && (
            <Message negative>{auth.error.message}</Message>
          )}
          <Form size="large" onSubmit={handleSubmit}>
            <Segment stacked>
              <Form.Input
                fluid
                icon="mail"
                iconPosition="left"
                placeholder="E-mail address"
                name="email"
                value={formValues.email}
                onChange={handleChange}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                name="password"
                value={formValues.password}
                onChange={handleChange}
              />

              <Button
                fluid
                size="large"
                type="submit"
                style={{ backgroundColor: "#F1574B", color: "#fff" }}
              >
                Login
              </Button>
            </Segment>
          </Form>
          <Message>
            New to us?{" "}
            <Link href="/register">
              <a>Sign Up</a>
            </Link>
          </Message>
        </Grid.Column>
      </Grid>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.app.auth,
});

const mapDispatchToProps = { login };

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
