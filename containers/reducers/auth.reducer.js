import { LOGIN, REGISTER, LOGOUT } from "../actions/types";
import { updateObject } from "../../utils/client/object.util";

const initialState = {
  isAuthenticated: false,
  loading: false,
  error: { status: false, message: "" },
};

function loginRequest(state, action) {
  return updateObject(state, { loading: true });
}

function loginSuccess(state, action) {
  return updateObject(state, {
    isAuthenticated: action.payload.isAuthenticated,
    error: { ...state.error, status: false, message: "" },
  });
}

function loginFailure(state, action) {
  return updateObject(state, {
    isAuthenticated: action.payload.isAuthenticated,
    error: {
      ...state.error,
      status: true,
      message: action.payload.error.message,
    },
  });
}

function loginFulfil(state, action) {
  return updateObject(state, { loading: false });
}

function registerRequest(state, action) {
  return updateObject(state, { loading: true });
}

function registerSuccess(state, action) {
  return updateObject(state, {
    isAuthenticated: action.payload.isAuthenticated,
    error: { ...state.error, status: false, message: "" },
  });
}

function registerFailure(state, action) {
  return updateObject(state, {
    isAuthenticated: action.payload.isAuthenticated,
    error: {
      ...state.error,
      status: true,
      message: action.payload.error.message,
    },
  });
}

function registerFulfil(state, action) {
  return updateObject(state, { loading: false });
}

function logoutUser(state, action) {
  return updateObject(state, { isAuthenticated: false });
}

export default (state = initialState, action) => {
  let { type } = action;

  switch (type) {
    // Login
    case LOGIN.REQUEST:
      return loginRequest(state, action);

    case LOGIN.SUCCESS:
      return loginSuccess(state, action);

    case LOGIN.FAILURE:
      return loginFailure(state, action);

    case LOGIN.FULFIL:
      return loginFulfil(state, action);

    // Register
    case REGISTER.REQUEST:
      return registerRequest(state, action);

    case REGISTER.SUCCESS:
      return registerSuccess(state, action);

    case REGISTER.FAILURE:
      return registerFailure(state, action);

    case REGISTER.FULFIL:
      return registerFulfil(state, action);

    // Logout
    case LOGOUT:
      return logoutUser(state, action);

    default:
      return state;
  }
};
