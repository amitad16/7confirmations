import React, { Component } from "react";
import Head from "next/head";

import Footer from "../components/Footer";
import { Icon } from "semantic-ui-react";

export default class Layout extends Component {
  render() {
    const { children, title, style, className } = this.props;

    return (
      <div className={"main-layout col " + className} style={style}>
        <Head>
          {/* <title>{title}</title> */}
          {process.env.NODE_ENV !== "production" && (
            <link
              rel="stylesheet"
              type="text/css"
              href={"/_next/static/css/styles.chunk.css?v=" + Date.now()}
            />
          )}
        </Head>
        <div className="main-content">{children}</div>

        <div className="whatsapp-contact">
          <a
            href="https://wa.me/919625134457?text=Hey! I am interested in 7Confirmations, can I know more about it?"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon name="whatsapp" size="huge" />
          </a>
        </div>
      </div>
    );
  }
}
