import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import { Container, Grid } from "semantic-ui-react";
import axios from "axios";

import styles from "./ResultPage.css";

import SimpleSection from "../../components/SimpleSection";
import Navigation from "../../components/Navigation/Navigation";
import PricePlans from "../../components/PricePlans";
import UpsDownDifferences from "../../components/UpsDownDifferences";

// Actions
import {
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
} from "../ProductPage/actions";
import { ProductCard } from "../../components/Cards";

const ResultPage = ({
  products,
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
}) => {
  const router = useRouter();
  const { user } = router.query;

  // console.log("router::", router);
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState({});

  const [windowDimensions, setWindowDimensions] = useState();
  const [productCols, setProductCols] = useState();

  useEffect(() => {
    getAllProducts();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);

    return () => {
      window.removeEventListener("resize", updateWindowDimensions);
    };
  }, []);

  function updateWindowDimensions() {
    setWindowDimensions({
      width: window.innerWidth,
      height: window.innerHeight,
    });

    if (window.innerWidth > 1024) setProductCols(3);
    else if (window.innerWidth > 742) setProductCols(4);
    else if (window.innerWidth > 500) setProductCols(3);
    else setProductCols(2);
  }

  function handleAddProductToCart(productId) {
    console.log("handle add prod");

    addProductsToCart(productId);
  }

  function handleRemoveProductFromCart(productId) {
    removeProductFromCart(productId);
  }

  useEffect(() => {
    if (user) {
      getUserResult(user);
    }
  }, [router.query.user]);

  async function getUserResult(userId) {
    try {
      setLoading(true);
      let { data } = await axios.get(`/api/v1/user/${userId}/result`);

      setResult(data);
    } catch (err) {
      console.log("error in getting user result::", err);
    } finally {
      setLoading(false);
    }
  }

  function getProductFromGroup(productId, productGroup, productType) {
    getProductByGroupCategory(productId, productGroup, productType);
  }

  return (
    <div className={styles.ResultPage}>
      <Navigation className="relative" />
      <Container className={`${styles.ResultPage__body}`}>
        <SimpleSection heading="Recommended Products">
          <Grid style={{ marginBottom: "50px" }}>
            <Grid.Row columns={productCols}>
              {products.map((v) => (
                <Grid.Column
                  key={v._id}
                  className={styles.ProductCard__wrapper}
                >
                  <ProductCard
                    {...v}
                    getProductFromGroup={getProductFromGroup}
                    addToCart={handleAddProductToCart}
                    removeFromCart={handleRemoveProductFromCart}
                  />
                </Grid.Column>
              ))}
            </Grid.Row>
          </Grid>
        </SimpleSection>

        {loading ? (
          <SimpleSection heading="Result">
            Preparing your results...
          </SimpleSection>
        ) : (
          <SimpleSection heading="Result" subHeading={result.natureType}>
            <p className={styles.ResultPage__natureExplaination}>
              {result.natureExplaination}
            </p>

            <UpsDownDifferences
              ups={result.upsAndDowns.ups}
              downs={result.upsAndDowns.downs}
            />

            <h3 className={styles.ResultPage__heading}>Some of your traits:</h3>
            <ul>
              {result.traits.map((v) => (
                <li key={v.id} className={styles.ResultPage__trait}>
                  {v.value}
                </li>
              ))}
            </ul>
          </SimpleSection>
        )}
        {/* Plans section */}

        <p className={styles.ResultPage__subheading}>
          Enroll in any plan now. Whatsapp or call at 8851395512 to know more
        </p>

        <PricePlans />
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({ products: state.products.items });
const mapDispatchToProps = {
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultPage);
