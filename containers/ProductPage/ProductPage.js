import { useEffect, useState } from "react";
import { connect } from "react-redux";
import Link from "next/link";

import { Grid, Container, Label } from "semantic-ui-react";

import styles from "./ProductPage.css";

import Header from "./Header";
import { ProductCard } from "../../components/Cards";
import SimpleSection from "../../components/SimpleSection";

// Actions
import {
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
} from "./actions";
import ReviewCard from "../../components/ReviewCard";
import FAQ from "../HomePage/sections/FAQ";

const reviews = [
  {
    id: "1",
    productName: "PMS Gummies",
    reviewBy: "Garima",
    review:
      "Lower back pain, cramps, and migraines are gone with 7C. Major plus is that my skin has cleared up...no more breakouts! Bloating and mood swings have improved too.",
  },
  {
    id: "2",
    productName: "PMS Gummies",
    reviewBy: "Meenu Sharma",
    review:
      "I've never used any type of supplement for anything, but took a chance because the reviews were great and my PMS is not! And I can honestly say, these work! I was surprised that they not only helped with my mood, but I have zero pre-period symptoms and it has helped with Acne. It took longer to notice that- 3 full cycles- but I broke out much less. I honestly would recommend these.",
  },
  {
    id: "3",
    productName: "Pcos",
    reviewBy: "Tara",
    review:
      "K, so they smell like those Trix yogurts and I'm all about it. They're super tasty, but really just took the edge off the hormones and horrendousness of my period. My hormones regulated like none other. Definitely ordering again",
  },
  {
    id: "4",
    productName: "Immunity",
    reviewBy: "Sushma K.",
    review:
      "I am a Police officer. So, I needed the immunity booster which is very needed at this time of distress and with 7C, I got it in the perfect way. The right blend of ayurveda and vitamins",
  },
];

const ProductPage = ({
  products,
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
  cartItemCount,
}) => {
  const [windowDimensions, setWindowDimensions] = useState();
  const [productCols, setProductCols] = useState();

  useEffect(() => {
    getAllProducts();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);

    return () => {
      window.removeEventListener("resize", updateWindowDimensions);
    };
  }, []);

  function updateWindowDimensions() {
    setWindowDimensions({
      width: window.innerWidth,
      height: window.innerHeight,
    });

    if (window.innerWidth > 1024) setProductCols(3);
    else if (window.innerWidth > 742) setProductCols(4);
    else if (window.innerWidth > 500) setProductCols(3);
    else setProductCols(2);
  }

  function handleAddProductToCart(productId) {
    addProductsToCart(productId);
  }

  function handleRemoveProductFromCart(productId) {
    removeProductFromCart(productId);
  }

  function getProductFromGroup(productId, productGroup, productType) {
    getProductByGroupCategory(productId, productGroup, productType);
  }

  return (
    <div className={styles.ProductPage}>
      <Header className="relative" />
      <Container>
        <Grid>
          <Grid.Row columns={productCols}>
            {products.map((v) => (
              <Grid.Column key={v._id} className={styles.ProductCard__wrapper}>
                <ProductCard
                  {...v}
                  getProductFromGroup={getProductFromGroup}
                  addToCart={handleAddProductToCart}
                  removeFromCart={handleRemoveProductFromCart}
                />
              </Grid.Column>
            ))}
          </Grid.Row>
        </Grid>
      </Container>

      <SimpleSection
        className={styles.ProductPage__whats_inside}
        heading="What's Inside?"
        subHeading="Powerful combo of vitamins &amp; ayurveda"
        style={{ marginTop: "100px", borderBottom: "none" }}
      >
        <p className={styles.ProductPage__whats_inside__para}>
          Say bye to bloating, cramps, headaches, hormonal acne, mood swings and
          nausea. Each gummy is filled with the goodness of both Vitamins (C, D
          &amp; Zinc) and Ayurveda (Musta, Lodhra, Ashoka, Brahmi, Shatavari)
          which not only helps in building the regular period flow but also
          protects you against PMS.
        </p>

        <p className={styles.ProductPage__whats_inside__para}>
          Combined with exercise and the right diet, 7C Blossom PCOS can bring
          amazing results. PCOS gummies/ drink has 14 essential micronutrients
          that nourish the hormones and 5 Ayurvedic herbs that provide the right
          environment for the effective functioning of the hormones. Herbs like
          Ashwagandha help to reduce stress and Shatavari, Queen of herbs,
          balances hormones and micronutrients like Vitamin C boosts immunity.
        </p>
      </SimpleSection>

      <SimpleSection heading="Customer Reviews" style={{ marginTop: "100px" }}>
        <Grid
          container
          columns={2}
          stackable
          className={styles.Reviews__wrapper}
        >
          {reviews.map((v) => (
            <Grid.Column key={v.id}>
              <ReviewCard
                productName={v.productName}
                userName={v.reviewBy}
                review={v.review}
              />
            </Grid.Column>
          ))}
        </Grid>
      </SimpleSection>

      <SimpleSection
        heading="FAQs"
        bgColor="#E1EAF4"
        style={{ marginBottom: "-100px" }}
      >
        <FAQ />
      </SimpleSection>

      <Link href="/cart">
        <a className={`btn-theme ${styles.FloatingCart}`}>
          Cart
          <Label
            color="teal"
            style={{
              position: "absolute",
              top: "-10px",
              right: "-10px",
            }}
          >
            {cartItemCount}
          </Label>
        </a>
      </Link>
    </div>
  );
};

const mapStateToProps = (state) => ({
  products: state.products.items,
  cartItemCount: state.products.items.filter((v) => v.addedToCart).length,
});
const mapDispatchToProps = {
  getAllProducts,
  getProductByGroupCategory,
  addProductsToCart,
  removeProductFromCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
