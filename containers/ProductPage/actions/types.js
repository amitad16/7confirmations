import createProtocol from "../../../utils/client/protocol.util";

export const GET_ALL_PRODUCTS = createProtocol("GET_ALL_PRODUCTS");

export const GET_PRODUCT_GROUP_TYPE = createProtocol("GET_PRODUCT_GROUP_TYPE");

export const ADD_TO_CART = createProtocol("ADD_TO_CART");

export const REMOVE_FROM_CART = createProtocol("REMOVE_FROM_CART");

export const GET_CART_PRODUCTS = createProtocol("GET_CART_PRODUCTS");

export const GET_LISTED_PRODUCTS = createProtocol("GET_LISTED_PRODUCTS");
