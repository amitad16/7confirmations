// Action Type
import { ADD_TO_CART } from "./types";

// Service
import { addProductToCart } from "../../../services/product.service";

export default (productId) => async (dispatch) => {
  try {
    dispatch({ type: ADD_TO_CART.REQUEST });

    let data = await addProductToCart(productId);

    console.log("action => get all products", data);

    dispatch({ type: ADD_TO_CART.SUCCESS, payload: { productId } });
  } catch (err) {
    console.log("cannot get all products", err);
    dispatch({ type: ADD_TO_CART.FAILURE });
  } finally {
    dispatch({ type: ADD_TO_CART.FULFIL });
  }
};
