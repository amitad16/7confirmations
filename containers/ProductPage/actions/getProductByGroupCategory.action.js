// Action Type
import { GET_PRODUCT_GROUP_TYPE } from "./types";

// Service
import { getProductGroupType } from "../../../services/product.service";

export default (productId, productGroup, productType) => async (dispatch) => {
  try {
    console.log("getting group item products");

    dispatch({ type: GET_PRODUCT_GROUP_TYPE.REQUEST });

    let data = await getProductGroupType(productGroup, productType);

    console.log("action => get group item products", data);

    dispatch({
      type: GET_PRODUCT_GROUP_TYPE.SUCCESS,
      payload: { item: data, productId },
    });
  } catch (err) {
    console.log("cannot get group item products", err);
    dispatch({ type: GET_PRODUCT_GROUP_TYPE.FAILURE });
  } finally {
    dispatch({ type: GET_PRODUCT_GROUP_TYPE.FULFIL });
  }
};
