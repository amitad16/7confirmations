// Action Type
import { GET_ALL_PRODUCTS } from "./types";

// Service
import { getAllProducts } from "../../../services/product.service";

export default () => async (dispatch) => {
  try {
    console.log("getting all products");

    dispatch({ type: GET_ALL_PRODUCTS.REQUEST });

    let data = await getAllProducts();

    console.log("action => get all products", data);

    dispatch({ type: GET_ALL_PRODUCTS.SUCCESS, payload: { items: data } });
  } catch (err) {
    console.log("cannot get all products", err);
    dispatch({ type: GET_ALL_PRODUCTS.FAILURE });
  } finally {
    dispatch({ type: GET_ALL_PRODUCTS.FULFIL });
  }
};
