import {
  GET_ALL_PRODUCTS,
  GET_PRODUCT_GROUP_TYPE,
  ADD_TO_CART,
  REMOVE_FROM_CART,
} from "../actions/types";
import {
  updateObject,
  updateItemInArray,
} from "../../../utils/client/object.util";

const initialState = {
  items: [],
  processing: {
    getAllProducts: false,
    getProductGroupItem: false,
    addProductToCart: false,
    removeProductFromCart: false,
    getListedProducts: false,
    getCartProducts: false,
  },
  error: {
    getAllProducts: "",
    getProductGroupItem: "",
    addProductToCart: "",
    removeProductFromCart: "",
    getListedProducts: "",
    getCartProducts: "",
  },
};

// Get all products
function getAllProductsRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getAllProducts: true },
    error: { ...state.error, getAllProducts: "" },
  });
}

function getAllProductsSucces(state, action) {
  return updateObject(state, action.payload);
}

function getAllProductsFaliure(state, action) {
  return updateObject(
    state,
    ...updateObject(state.error, { getAllProducts: action?.payload?.error })
  );
}

function getAllProductsFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getAllProducts: false },
  });
}

// Get product group item
function getProductGroupItemRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getProductGroupItem: true },
    error: { ...state.error, getProductGroupItem: "" },
  });
}

function getProductGroupItemSucces(state, action) {
  return updateObject(state, {
    items: updateItemInArray(
      state.items,
      action.payload.productId,
      "_id",
      (item) => action.payload.item
    ),
  });
}

function getProductGroupItemFaliure(state, action) {
  return updateObject(
    state,
    ...updateObject(state.error, { getProductGroupItem: action.payload.error })
  );
}

function getProductGroupItemFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, getProductGroupItem: false },
  });
}

// Add To cart
function addToCartRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, addProductToCart: true },
    error: { ...state.error, addProductToCart: "" },
  });
}

function addToCartSucces(state, action) {
  return updateObject(state, {
    items: updateItemInArray(
      state.items,
      action.payload.productId,
      "_id",
      (item) => {
        return { ...item, addedToCart: true };
      }
    ),
  });
}

function addToCartFaliure(state, action) {
  return updateObject(state, {
    error: { ...state.error, addProductToCart: action.payload.error },
  });
}

function addToCartFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, addProductToCart: false },
  });
}

// Remove from cart
function removeFromCartRequest(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, removeProductFromCart: true },
    error: { ...state.error, removeProductFromCart: "" },
  });
}

function removeFromCartSucces(state, action) {
  return updateObject(state, {
    items: updateItemInArray(
      state.items,
      action.payload.productId,
      "_id",
      (item) => {
        return { ...item, addedToCart: false };
      }
    ),
  });
}

function removeFromCartFaliure(state, action) {
  return updateObject(state, {
    error: { ...state.error, removeProductFromCart: action.payload.error },
  });
}

function removeFromCartFulfil(state, action) {
  return updateObject(state, {
    processing: { ...state.processing, removeProductFromCart: false },
  });
}

export default (state = initialState, action) => {
  let { type } = action;

  switch (type) {
    // Get all products
    case GET_ALL_PRODUCTS.REQUEST:
      return getAllProductsRequest(state, action);

    case GET_ALL_PRODUCTS.SUCCESS:
      return getAllProductsSucces(state, action);

    case GET_ALL_PRODUCTS.FAILURE:
      return getAllProductsFaliure(state, action);

    case GET_ALL_PRODUCTS.FULFIL:
      return getAllProductsFulfil(state, action);

    // Get product group item
    case GET_PRODUCT_GROUP_TYPE.REQUEST:
      return getProductGroupItemRequest(state, action);

    case GET_PRODUCT_GROUP_TYPE.SUCCESS:
      return getProductGroupItemSucces(state, action);

    case GET_PRODUCT_GROUP_TYPE.FAILURE:
      return getProductGroupItemFaliure(state, action);

    case GET_PRODUCT_GROUP_TYPE.FULFIL:
      return getProductGroupItemFulfil(state, action);

    // Add to cart
    case ADD_TO_CART.REQUEST:
      return addToCartRequest(state, action);

    case ADD_TO_CART.SUCCESS:
      return addToCartSucces(state, action);

    case ADD_TO_CART.FAILURE:
      return addToCartFaliure(state, action);

    case ADD_TO_CART.FULFIL:
      return addToCartFulfil(state, action);

    // remove from cart
    case REMOVE_FROM_CART.REQUEST:
      return removeFromCartRequest(state, action);

    case REMOVE_FROM_CART.SUCCESS:
      return removeFromCartSucces(state, action);

    case REMOVE_FROM_CART.FAILURE:
      return removeFromCartFaliure(state, action);

    case REMOVE_FROM_CART.FULFIL:
      return removeFromCartFulfil(state, action);

    default:
      return state;
  }
};
