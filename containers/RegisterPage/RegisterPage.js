import { useState } from "react";
import { connect } from "react-redux";

import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
} from "semantic-ui-react";
import Link from "next/link";

import { register } from "../actions";
import Navigation from "../../components/Navigation/Navigation";

const RegisterPage = ({ auth, register }) => {
  const [formValues, setFormValues] = useState({
    fullname: "",
    email: "",
    password: "",
  });

  function handleChange(_, data) {
    setFormValues((prevState) => ({ ...prevState, [data.name]: data.value }));
  }

  function handleSubmit(e) {
    e.preventDefault();

    console.log("formValues = ", formValues);

    register(formValues);
  }

  return (
    <div>
      <Navigation />
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" textAlign="center" style={{ color: "#F1574B" }}>
            Let's get started
          </Header>
          {auth.error.status && (
            <Message negative>{auth.error.message}</Message>
          )}
          <Form size="large" onSubmit={handleSubmit}>
            <Segment stacked>
              <Form.Input
                fluid
                icon="user"
                iconPosition="left"
                placeholder="Full Name"
                name="fullname"
                value={formValues.fullname}
                onChange={handleChange}
              />

              <Form.Input
                fluid
                icon="mail"
                iconPosition="left"
                placeholder="E-mail address"
                name="email"
                value={formValues.email}
                onChange={handleChange}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                name="password"
                value={formValues.password}
                onChange={handleChange}
              />

              <Button
                color="teal"
                fluid
                size="large"
                type="submit"
                style={{ backgroundColor: "#F1574B", color: "#fff" }}
              >
                Register
              </Button>
            </Segment>
          </Form>
          <Message>
            Have an account?{" "}
            <Link href="/login">
              <a>Login</a>
            </Link>
          </Message>
        </Grid.Column>
      </Grid>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.app.auth,
});

const mapDispatchToProps = { register };

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
