import React from "react";

import Slider from "react-slick";

import styles from "../HomePage.css";
import Review from "./Review";

const reviews = [
  {
    id: "1",
    review:
      "I have PCOS and my symptoms include horrible mood swings, depression, acne from stress that is brought on by depression. Since taking these, I have stopped crying incessantly the week before my period, stopped having the rampant mood swings and lessened my stress and depression. With that, the acne dissipated. These are worth their weight in gold",
    name: "Sandhya",
    // headline: "Deepak's plan",
    // target: "Improve sexual energy",
    // routines: [
    //   { id: "1", task: "Right balance diet, right energy" },
    //   { id: "2", task: "Yoga (and pranayam)  to build stamina" },
    //   { id: "3", task: "Ashwangandha + milk everyday before sleep." },
    // ],
  },
  {
    id: "2",
    review:
      "I haven't had a debilitating period in 3 months. No migraines, no feeling sick before and the first day of my period, no more cramps. These are a Godsend!! And in combination with Jubilance i am finally feeling ‘normal'. No more PMDD",
    name: "Anurima",
    // headline: "Madhav's plan",
    // target: "Improve eye sight",
    // routines: [
    //   { id: "1", task: "Awla a day." },
    //   { id: "2", task: "Remainders to wash eyes every 3 hours." },
    //   { id: "3", task: "Mahatriphla grit in night" },
    // ],
  },
  {
    id: "3",
    review: `I no longer have "adult" acne. And my periods are lighter with light pain on the first day, if any. These ayurvedic herbs works wonderful`,
    name: "Shivani",
    // headline: "Garima's plan",
    // target: "Improve memory",
    // routines: [
    //   { id: "1", task: "Memory test everyday" },
    //   { id: "2", task: "Brahmarasayna everyday" },
    //   { id: "3", task: "Brahmigrit + milk before sleep" },
    // ],
  },
];

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  // dotsClass: `${styles.ReviewCarousel__dots} ReviewCarousel__dots`,
  // responsive: [
  //   {
  //     breakpoint: 1024,
  //     settings: {
  //       slidesToShow: 3,
  //       slidesToScroll: 3,
  //       infinite: true,
  //       dots: true,
  //     },
  //   },
  //   {
  //     breakpoint: 768,
  //     settings: {
  //       slidesToShow: 2,
  //       slidesToScroll: 2,
  //       initialSlide: 2,
  //     },
  //   },
  //   {
  //     breakpoint: 580,
  //     settings: {
  //       slidesToShow: 1,
  //       slidesToScroll: 1,
  //     },
  //   },
  // ],
};

const ReviewCarousel = () => {
  return (
    <div className={styles.ReviewCarousel}>
      <Slider {...settings}>
        {reviews.map((v) => (
          <Review
            key={v.id}
            // headline={v.headline}
            name={v.name}
            review={v.review}
            // target={v.target}
            // routines={v.routines}
          />
        ))}
      </Slider>
    </div>
  );
};

export default ReviewCarousel;
