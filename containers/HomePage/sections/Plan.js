import React from "react";

import styles from "../HomePage.css";

const Plan = ({ headline, target, routines }) => {
  return (
    <div className={styles.Plan}>
      <div className={styles.Plan__headline}>{headline}</div>
      <div className={styles.Plan__target}>{target}</div>
      <div className={styles.Plan__routine_wrapper}>
        {routines.map((routine) => (
          <div key={routine.id} className={styles.Plan__routine}>
            {routine.task}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Plan;
