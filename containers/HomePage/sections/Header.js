import React from "react";

import styles from "../HomePage.css";

import Navigation from "../../../components/Navigation/Navigation";
import BigCarousel from "./BigCarousel";

const Header = () => {
  return (
    <div className={`ui inverted masthead centered segment ${styles.Header}`}>
      <div
        className="ui page"
        style={{ height: "100%", backgroundColor: "#ffffff87" }}
      >
        <div className="column">
          <Navigation />
        </div>
        <BigCarousel />
      </div>
    </div>
  );
};

export default Header;
