import { Component, Fragment } from "react";
import { Accordion, Icon } from "semantic-ui-react";

import styles from "../HomePage.css";

const faqs = [
  {
    id: "1",
    question: "How does this work?",
    answer: `Let us tell you what goes on behind the scenes. Please don’t tell anyone else. It’s our secret. Promise US?


    The principle of Ayurveda is based on characteristic classification of human beings. Depending on that, when you fill out the form, we get to know your metabolic energy or nature (known as dosha). Depending on your particular set of traits, we made personalised products for you, which no one else currently offers you in the world. 
     
    No two people are the same. What works for you is different from what works for your bff.
    
    With the right set of herbs, made just for you, you will start seeing the results not only in your menstrual cycle, but also your skin and hair. Because when you are beautiful within, you are beautiful on the outside.`,
  },
  {
    id: "2",
    question: "How often will this box show up at my door?",
    answer: `We recommend you monthly. Once you try out our products for at least 3 months, you will start seeing the magic (though it will take less time than that but we all are different, we respond differently, so let’s take the time which works for everyone). Once you try it out, we know you will come back for the rest of your life.`,
  },
  {
    id: "3",
    question: "How safe are our products?",
    answer:
      "Our products are made of 100% natural ingredients and thus very safe. Ayurvedic herbs, have you heard of them? They are the best, right from heaven. Synthetic chemicals can have serious side effects but with our products, we will reward you Rs 1 Lakh if you find any synthetic chemicals. Give yourself the care you deserve.",
  },
  {
    id: "4",
    question: "I am on birth control pills? Is it safe for me?",
    answer:
      "We would say yes. But, it’s better to be safe. If you are on birth control or any medical condition, we recommend consulting a physician before using the products.",
  },
  {
    id: "5",
    question: "Are there any cancelling or any other stupid charges?",
    answer:
      "No way. We don't like any extra or stupid fees. Our favourite is you. You only pay for what we ship you.",
  },
  {
    id: "6",
    question: "Why do I have to consume it regularly?",
    answer:
      "Actually the thing is there are hormonal changes all over the month. And we are focusing on balancing those hormones. Let us give you an example. Can you make those sexy biceps, just by going to Gym one day? You know you can’t. You have to workout every single day. Sobaby, consume it everyday and you will see the desired effects in no time. Give yourself the care you deserve.",
  },
  {
    id: "7",
    question: "Aren’t these products slightly expensive?",
    answer:
      "We were waiting for this one. How do you think you can get a toffee for 1 rupee? There are 100s of people involved in the process and still you get that for Re. 1? Doesn't it sound strange. It is more dangerous for you than you can ever think of. We consider you our bff, we can’t do that for you. What makes us slightly expensive is our promise to stick to natural and healthy ingredients. Making it personalised and healthy at same time, do you still think it’s expensive? Give yourself the care you deserve.",
  },
  {
    id: "8",
    question: "What if it doesn’t work for me?",
    answer:
      "If that happens, we would fail and we never wanna fail. Had you a friend in school, who got to cry for one mark even after getting 1st rank. Yeah, we were that kid. We always thrive for the best and never settle for anything less than that. We promise you money back guarantee, and something we hate more than advertisements in our favourite show is returning money back. So, we will make it work for you and somehow god forbid, if not, only for you, just you, we will give your money back and that too with interest.",
  },
];
class FAQ extends Component {
  state = { activeIndex: 0 };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  render() {
    const { activeIndex } = this.state;

    return (
      <Accordion className={styles.FAQ__wrapper}>
        {faqs.map((v, i) => (
          <div key={v.id} className={styles.FAQ__item}>
            <Accordion.Title
              className={styles.FAQ__item__title}
              active={activeIndex === i}
              index={i}
              onClick={this.handleClick}
            >
              <Icon name="dropdown" />
              {v.question}
            </Accordion.Title>
            <Accordion.Content
              className={styles.FAQ__item__content}
              active={activeIndex === i}
            >
              <p>{v.answer}</p>
            </Accordion.Content>
          </div>
        ))}
      </Accordion>
    );
  }
}

export default FAQ;
