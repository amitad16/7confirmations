import React from "react";

import styles from "../HomePage.css";

const Review = ({ review, name }) => {
  return (
    <div className={styles.Review}>
      <div className={styles.Review__content}>{review}</div>
      <div className={styles.Review__name}>-{name}</div>
      {/* <div className={styles.Review__headline}>{headline}</div> */}
      {/* <div className={styles.Review__target}>{target}</div> */}
      {/* <div className={styles.Review__routine_wrapper}>
        {routines.map((routine) => (
          <div key={routine.id} className={styles.Review__routine}>
            {routine.task}
          </div>
        ))}
      </div> */}
    </div>
  );
};

export default Review;
