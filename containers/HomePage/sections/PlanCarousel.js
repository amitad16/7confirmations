import React from "react";

import Slider from "react-slick";

import styles from "../HomePage.css";
import Plan from "./Plan";

// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

const plans = [
  {
    id: "1",
    headline: "Deepak's plan",
    target: "Improve sexual energy",
    routines: [
      { id: "1", task: "Right balance diet, right energy" },
      { id: "2", task: "Yoga (and pranayam)  to build stamina" },
      { id: "3", task: "Ashwangandha + milk everyday before sleep." },
    ],
  },
  {
    id: "2",
    headline: "Madhav's plan",
    target: "Improve eye sight",
    routines: [
      { id: "1", task: "Awla a day." },
      { id: "2", task: "Remainders to wash eyes every 3 hours." },
      { id: "3", task: "Mahatriphla grit in night" },
    ],
  },
  {
    id: "3",
    headline: "Garima's plan",
    target: "Improve memory",
    routines: [
      { id: "1", task: "Memory test everyday" },
      { id: "2", task: "Brahmarasayna everyday" },
      { id: "3", task: "Brahmigrit + milk before sleep" },
    ],
  },
];

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dotsClass: `${styles.PlanCarousel__dots} PlanCarousel__dots`,
};

const PlanCarousel = () => {
  return (
    <div className={styles.PlanCarousel}>
      <Slider {...settings}>
        {plans.map((plan) => (
          <Plan
            key={plan.id}
            headline={plan.headline}
            target={plan.target}
            routines={plan.routines}
          />
        ))}
      </Slider>
    </div>
  );
};

export default PlanCarousel;
