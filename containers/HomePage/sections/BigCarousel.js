import Link from "next/link";
import Typewriter from "typewriter-effect";

import styles from "../HomePage.css";

const BigCarousel = () => {
  return (
    <div className={styles.BigCarousel}>
      <div className={styles.Header__content}>
        <div className={styles.Header__content_title_wrapper}>
          <p className={styles.Header__content_title}>Say Bye To</p>
          <br />
          <div
            className={`${styles.Header__content_title} ${styles.Header__content_title_2}`}
          >
            <Typewriter
              options={{
                strings: [
                  "PCOS",
                  "Cramps",
                  "Bloating",
                  "Headache",
                  "Hormonal Acne",
                  "Mood Swings",
                  "Irregular Periods",
                  "PMS",
                ],
                autoStart: true,
                loop: true,
              }}
            />
          </div>
        </div>

        {/* <p className={styles.Header__content_subtitle}>
          Happier and healthier periods. When baby, you are healthy inside, you
          are beautiful outside.
        </p> */}
        {/* <div
          className={`${styles.Header__content_subtitle} ${styles.Header__content_list}`}
        >
          <p>Cramps&nbsp;&nbsp;•&nbsp;&nbsp;</p>
          <p>Bloating&nbsp;&nbsp;•&nbsp;&nbsp;</p>
          <p>Acne&nbsp;&nbsp;•&nbsp;&nbsp;</p>
          <p>Mood Swings</p>
        </div> */}
      </div>
      <div className={styles.BigCarousel__text_container_static}>
        <Link href="/survey/new">
          <a>
            <button
              className={`btn__know-about-you btn-theme btn-large ${styles.BigCarousel__know_about_you_btn}`}
            >
              Get Started
            </button>
          </a>
        </Link>
        <br />
        {/* <p className={styles.BigCarousel__text_static}>
          Daily Challange, Daily Care!
        </p> */}
      </div>
      {/* <Slider {...settings}>
        {slideContent.map((v) => (
          <div
            key={v.id}
            className={styles.BigCarousel__slide_wrapper}
            // style={{ backgroundImage: `url(${v.image})` }}
          >
            <div
              className={`${styles.BigCarousel__slide_bg} BigCarousel__slide_bg${v.id}`}
            ></div>
            <div className={styles.BigCarousel__slide_text_container}>
              <p
                className={styles.BigCarousel__slide_text_primary}
                style={{ color: v.textPrimaryColor }}
              >
                {v.textPrimary}
              </p>
              <p
                className={styles.BigCarousel__slide_text_secondary}
                style={{ color: v.textSecondaryColor }}
              >
                {v.textSecondary}
              </p>
            </div>
          </div>
        ))}
      </Slider> */}
    </div>
  );
};

export default BigCarousel;
