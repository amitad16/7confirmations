import React from "react";
import { Grid, Icon } from "semantic-ui-react";

import styles from "./HomePage.css";

import Header from "./sections/Header";
import SimpleSection from "../../components/SimpleSection";
import PlanCarousel from "./sections/PlanCarousel";
import Footer from "../../components/Footer";
import Link from "next/link";
import FAQ from "./sections/FAQ";
import ReviewCarousel from "./sections/ReviewsCarousel";

const HomePage = () => {
  return (
    <div id="home" className={styles.HomePage}>
      <Header />

      <SimpleSection heading="The Care you Deserve" bgColor="#ffffff">
        <p className={styles.Section__content}>
          <span className="FocusWord">350 Crores+ females</span> worldwide -
          Teachers, Sportsperson, Doctors, Nurses, Moms, Housewives, You and US.{" "}
          <span className="FocusWord">What all do we share?</span> A rishta. A
          rishta of blood and love that gets stronger each month.
        </p>
        <p className={styles.Section__content}>
          So, let’s come together and give{" "}
          <span className="FocusWord">periods care to everyone.</span>
        </p>

        <div style={{ marginTop: "64px" }}>
          <Link href="/survey/new">
            <a>
              <button
                className={`btn__know-about-you btn-theme btn-large ${styles.BigCarousel__know_about_you_btn}`}
              >
                Take the QUIZ
              </button>
            </a>
          </Link>
        </div>
      </SimpleSection>

      <SimpleSection>
        <p className={styles.Section__heading}>
          Over 500 periods in our lifetime.{" "}
          <span className="FocusWord">Do we want</span> 500+{" "}
          <span className="FocusWord">cramps</span>, 500+{" "}
          <span className="FocusWord">bloated stomachs</span>, 500+{" "}
          <span className="FocusWord">headaches?</span>
        </p>
        <p style={{ fontSize: "6em", fontWeight: "800" }}>NO</p>
      </SimpleSection>

      <div
        style={{
          backgroundImage: "url(/img/box.png)",
          backgroundRepeat: "no-repeat",
          height: "350px",
          width: "100vw",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      ></div>

      <SimpleSection
        bgColor="#ffe9eb"
        heading="Want Healthier &amp; Regular periods?"
        subHeading="Try us out and if we couldn’t do that for you, we will return your
        money back. Batman Promise."
      ></SimpleSection>

      <SimpleSection>
        <Grid stackable relaxed>
          <Grid.Column
            width={10}
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <SimpleSection>
              <p className={styles.Section__heading}>
                <span className="FocusWord">Personalised</span>
                <br />
                Just for <br />
                <br />
                <span
                  style={{
                    fontSize: "2.5em",
                    fontWeight: "800",
                    color: "#f1574b",
                  }}
                >
                  YOU
                </span>
              </p>
            </SimpleSection>
          </Grid.Column>
          <Grid.Column
            width={6}
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <div
              className={`${styles.Section__content} ${styles.Section_card}`}
              style={{
                marginBottom: "36px",
              }}
            >
              <p style={{ fontSize: "2em", fontWeight: "600" }}>
                Made for <span className="FocusWord">YOU</span>,
              </p>
              <div style={{ textAlign: "left" }}>
                <p>Who just hit her 1st one (super excited!!)</p>
                <p>
                  Our baby who just hit her 20’s and is ready for your first
                  solo trip(get ready for that)
                </p>
                <p>
                  Mam who just came to know about your PCOS (we will beat it
                  together)
                </p>
                <p>
                  Our beautiful aunt who just hit her menopause(let’s go through
                  this new phase together)
                </p>
              </div>
            </div>
          </Grid.Column>
        </Grid>

        <Link href="/survey/new">
          <a>
            <button
              className={`btn__know-about-you btn-theme btn-large ${styles.BigCarousel__know_about_you_btn}`}
            >
              Take the QUIZ
            </button>
          </a>
        </Link>
      </SimpleSection>

      <SimpleSection
        heading="Why 7C?"
        subHeading="Formulated from natural ingredients and backed by ayurveda.
              Bringing 5000 years old traditions to your little vag*na."
      ></SimpleSection>

      <SimpleSection
        heading="100+ Doctors Recommend It"
        subHeading="Over 100 doctors in India &amp; USA recommend our FDA approved products for PMS Period Care &amp; Sexual Wellness"
        bgColor="#ffffff"
      >
        <>
          <img
            src="/img/doctor.png"
            alt="Doctor"
            style={{ maxWidth: "600px", width: "100%" }}
          />
          <div className={`${styles.ShapedContent__wrapper}`}>
            <div
              className={`${styles.ShapedContent__item} ${styles.ShapedContent__rect}`}
            >
              Sanitised Packaging
            </div>

            <div
              className={`${styles.ShapedContent__item} ${styles.ShapedContent__rect}`}
            >
              Safe Shipping
            </div>

            <div
              className={`${styles.ShapedContent__item} ${styles.ShapedContent__rect}`}
            >
              Contactless Delivery
            </div>
          </div>
        </>
      </SimpleSection>

      <SimpleSection>
        <p className={styles.Section__heading}>
          <span
            className="FocusWord"
            style={{
              fontSize: "1.5em",
              fontWeight: "800",
            }}
          >
            MISSION
          </span>
        </p>
        <p className={styles.Section__content}>
          We only love two people in this world. First is Batman and Second is
          you. We love you more than we love batman, sachi muchi and we are here
          for you. For those moments, when you say OMG! WHY ME? We want to help
          you say TRY ME. We love you, our BFF. Love your periods, NOT pms, NOT
          PCOS.
        </p>
      </SimpleSection>

      <SimpleSection heading="Reviews" bgColor="#ffffff">
        <ReviewCarousel />
      </SimpleSection>

      <SimpleSection
        heading="FAQs"
        bgColor="#E1EAF4"
        style={{ marginBottom: "-100px" }}
      >
        <FAQ />
      </SimpleSection>
    </div>
  );
};

export default HomePage;
