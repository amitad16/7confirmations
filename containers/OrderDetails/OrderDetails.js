import { useEffect, useState } from "react";
import { Segment, Header, Icon, Button, Grid } from "semantic-ui-react";
import moment from "moment";

import { getOrderStatus } from "../../services/order.service";

import styles from "./OrderDetails.css";
import Navigation from "../../components/Navigation/Navigation";
import Link from "next/link";

const OrderDetails = ({ orderId, orderStatus }) => {
  const [orderDetails, setOrderDetails] = useState(null);
  const [transactionStatus, setTransactionStatus] = useState("");

  useEffect(() => {
    if (orderId) {
      handleGetOrderStatus(orderId);
    }
  }, [orderId]);

  async function handleGetOrderStatus(orderId) {
    let data = await getOrderStatus(orderId);

    let _orderDetails = {
      "Customer Id": data.customerId,
      "Order Id": data.orderId,
      "Transaction Id": data.transactionDetails.TXNID,
      "Bank Name": data.transactionDetails.BANKNAME,
      "Bank Transaction Id": data.transactionDetails.BANKTXNID,
      Currency: data.transactionDetails.CURRENCY,
      "Geteway Name": data.transactionDetails.GATEWAYNAME,
      "Payment Mode": data.transactionDetails.PAYMENTMODE,
      "Transaction Amount": data.transactionDetails.TXNAMOUNT,
      "Transaction Date": moment(data.transactionDetails.TXNDATE).format(
        "LLLL"
      ),
    };

    setTransactionStatus(data.transactionDetails.RESPCODE);

    setOrderDetails(_orderDetails);
  }

  console.log("reder");

  return (
    <div className={styles.OrderDetails}>
      <Navigation className="relative" />
      <Segment placeholder className={styles.OrderDetails__segment}>
        {orderStatus === "fail" ? (
          <>
            <Header icon>
              <Icon className={styles.StatusMark} name="close" color="red" />
              <p className={styles.FailText}>Transaction Failed!</p>
            </Header>

            <br />
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <Link href="/">
                <a>
                  <Button basic color="red">
                    Back to Homepage
                  </Button>
                </a>
              </Link>

              <Link href="/cart">
                <a>
                  <Button basic color="red">
                    Goto Cart
                  </Button>
                </a>
              </Link>
            </div>
          </>
        ) : transactionStatus && transactionStatus === "01" ? (
          orderDetails && (
            <>
              <Header icon>
                <Icon
                  className={styles.StatusMark}
                  name="check circle outline"
                  color="green"
                />
                <p className={styles.SuccessText}>Transaction Successfull</p>
              </Header>

              <Grid>
                {Object.keys(orderDetails).map((v, i) => (
                  <Grid.Row columns={2} key={i}>
                    <Grid.Column className={styles.OrderDetails__property_name}>
                      {v}:
                    </Grid.Column>
                    <Grid.Column
                      className={styles.OrderDetails__property_value}
                    >
                      {orderDetails[v]}
                    </Grid.Column>
                  </Grid.Row>
                ))}
              </Grid>
              <br />
              <Link href="/">
                <a>
                  <Button color="green">Back to Homepage</Button>
                </a>
              </Link>
            </>
          )
        ) : (
          orderDetails && (
            <>
              <Header icon>
                <Icon className={styles.StatusMark} name="close" color="red" />
                <p className={styles.FailText}>Transaction Failed!</p>
              </Header>

              <br />
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-evenly",
                }}
              >
                <Link href="/">
                  <a>
                    <Button basic color="red">
                      Back to Homepage
                    </Button>
                  </a>
                </Link>

                <Link href="/cart">
                  <a>
                    <Button basic color="red">
                      Goto Cart
                    </Button>
                  </a>
                </Link>
              </div>
            </>
          )
        )}
      </Segment>
    </div>
  );
};

export default OrderDetails;
