import Surveyed from "../../components/Surveyed/Surveyed.js";
import axios from "axios";

// import styles from "./SurveyPage.css";

// import {
//   initializeSurvey,
//   postQuestionResponse,
//   getPreviousStep,
// } from "./data-sets/survey2/survey2-questions";

import Navigation from "../../components/Navigation/Navigation";

class SurveyPage extends React.PureComponent {
  constructor() {
    super();

    this.state = { step: null };

    this.handleOnStarting = this.handleOnStarting.bind(this);
    this.handleOnReady = this.handleOnReady.bind(this);
    this.handleOnEmpty = this.handleOnEmpty.bind(this);
    this.handleOnCompleted = this.handleOnCompleted.bind(this);
    this.handleOnBack = this.handleOnBack.bind(this);
    this.handleOnNext = this.handleOnNext.bind(this);
    this.initializeSurvey = this.initializeSurvey.bind(this);
  }

  componentDidMount() {
    this.initializeSurvey("a");
  }

  async initializeSurvey(surveyId) {
    try {
      let { data } = await axios.get(`/api/v1/survey/${surveyId}/initialize`);

      console.log("data::::", data);

      this.setState({ step: data, clientId: data.clientId });
    } catch (err) {
      console.log("errr initializeSurvey::", err);
    }
  }

  handleOnStarting() {
    console.log("onStarting");
  }

  handleOnReady(surveyCredentials) {
    console.log("onReady::", surveyCredentials);
  }

  handleOnEmpty() {
    console.log("onEmpty");
  }

  async handleOnCompleted({ surveyId, clientId }) {
    console.log("onCompleted");
    try {
      await axios.post(`/api/v1/survey/${surveyId}/complete`, {
        userId: clientId,
      });
    } catch (err) {
      console.log("errr handleOnBack::", err);
    }
  }

  async handleOnBack({ surveyId, clientId }) {
    console.log("onBack");
    try {
      let { data } = await axios.post(`/api/v1/survey/${surveyId}/prev`, {
        userId: clientId,
      });

      this.setState({ step: data });
    } catch (err) {
      console.log("errr handleOnBack::", err);
    }
  }

  async handleOnNext(response) {
    console.log("onNext::", response);

    try {
      let { surveyId, clientId, question_identifier, user_response } = response;

      let { data } = await axios.post(`/api/v1/survey/${surveyId}/next`, {
        userId: clientId,
        response: {
          question_identifier,
          user_response,
        },
      });

      this.setState({ step: data });
    } catch (err) {
      console.log("errr handleOnNext::", err);
    }
  }

  render() {
    return (
      <div className="">
        <Navigation />
        <div style={{ height: "100vh", width: "100vw" }}>
          <Surveyed
            onStarting={this.handleOnStarting}
            onReady={this.handleOnReady}
            onEmpty={this.handleOnEmpty}
            onCompleted={this.handleOnCompleted}
            onBack={this.handleOnBack}
            onNext={this.handleOnNext}
            surveyId="a"
            step={this.state.step}
            clientId={this.state.clientId}
          />
        </div>
      </div>
    );
  }
}

export default SurveyPage;
