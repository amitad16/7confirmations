// Utils
import { login } from "../../utils/auth.util";

// Services
import authService from "../../services/auth.service";

// Action Type
import { LOGIN } from "./types";

export default (userData, redirectTo) => async (dispatch) => {
  try {
    dispatch({ type: LOGIN.REQUEST });

    let data = await authService.login(userData);

    console.log("data auth service login = ", data);

    dispatch({
      type: LOGIN.SUCCESS,
      payload: { isAuthenticated: true },
    });

    login(
      {
        accessToken: data.accessToken,
        accessTokenExpiry: data.accessTokenExpiry,
      },
      false,
      redirectTo
    );
  } catch (err) {
    console.log("err message = ", err.message);

    console.log("errrr auth:::", err.response);

    let error =
      (err.response && err.response.data && err.response.data.message) ||
      err.message;

    dispatch({
      type: LOGIN.FAILURE,
      payload: { isAuthenticated: false, error: { message: error } },
    });
  } finally {
    dispatch({ type: LOGIN.FULFIL });
  }
};
