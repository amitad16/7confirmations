import createProtocol from "../../utils/client/protocol.util";

export const LOGIN = createProtocol("App/LOGIN");

export const REGISTER = createProtocol("App/REGISTER");

export const LOGOUT = "App/LOGOUT";
