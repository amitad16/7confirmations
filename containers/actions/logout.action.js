// Action Type
import { LOGOUT } from "./types";

export default () => async (dispatch) => {
  localStorage.removeItem("__user__");

  dispatch({ action: LOGOUT });
};
