// Services
import authService from "../../services/auth.service";

import { login } from "../../utils/auth.util";

// Action Type
import { REGISTER } from "./types";

export default (userData) => async (dispatch) => {
  try {
    dispatch({ type: REGISTER.REQUEST });

    let data = await authService.register(userData);

    console.log("data auth service register = ", data);

    dispatch({
      type: REGISTER.SUCCESS,
      payload: { isAuthenticated: true },
    });

    login({
      accessToken: data.accessToken,
      accessTokenExpiry: data.accessTokenExpiry,
    });
  } catch (err) {
    console.log("errrr auth:::", err);

    let error =
      (err.response && err.response.data && err.response.data.message) ||
      err.message;

    dispatch({
      type: REGISTER.FAILURE,
      payload: { isAuthenticated: false, error: { message: error } },
    });
  } finally {
    dispatch({ type: REGISTER.FULFIL });
  }
};
