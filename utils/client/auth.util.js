export const getUserToken = () => {
  let user = localStorage.getItem("__user__");

  if (!user) return "";

  user = JSON.parse(user);

  return user.accessToken;
};

export const getLoggedInUser = () => {
  let user = localStorage.getItem("__user__");

  if (!user) return false;

  user = JSON.parse(user);

  delete user.accessToken;

  return user.user;
};

export function removeLoggedinUser() {
  localStorage.removeItem("__user__");
}
