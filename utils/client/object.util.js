export function updateObject(oldObject, newValues) {
  return { ...oldObject, ...newValues };
}

export function updateItemInArray(
  array,
  itemId,
  itemIdPropertyName,
  updateItemCallback
) {
  const updatedItems = array.map((item) => {
    if (item[itemIdPropertyName] !== itemId) {
      // Since we only want to update one item, preserve all others as they are now
      return item;
    }
    // Use the provided callback to create an updated item
    const updatedItem = updateItemCallback(item);
    return updatedItem;
  });
  return updatedItems;
}

export function removeItemByIndexInArray(
  array,
  propertyName,
  propertyValue,
  deleteCount = 1
) {
  let idx = array.findIndex((v) => v[propertyName] === propertyValue);

  if (idx > -1) {
    array.splice(idx, deleteCount);
  }

  return array;
}
