export default function createProtocol(actionType) {
  return {
    TRIGGER: `${actionType}_TRIGGER`,
    REQUEST: `${actionType}_REQUEST`,
    SUCCESS: `${actionType}_SUCCESS`,
    FAILURE: `${actionType}_FAILURE`,
    FULFIL: `${actionType}_FULFIL`
  };
}
