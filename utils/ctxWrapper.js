import { Component } from "react";

const getDisplayName = (Component) =>
  Component.displayName || Component.name || "Component";

function withHostname(WrappedComponent) {
  return class extends Component {
    static displayName = `withHostname(${getDisplayName(WrappedComponent)})`;

    static async getInitialProps(ctx) {
      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx));

      const hostname = "http://localhost:8080";
      // const hostname = "https://7confirmations.xyz";
      return { ...componentProps, ...{ hostname } };
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

export { withHostname };
