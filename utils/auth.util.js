import { Component } from "react";
import Router from "next/router";
import fetch from "isomorphic-unfetch";
// import nextCookie from "next-cookies";

import { setAccessToken, getAccessToken } from "./accessToken.util";

// let inMemoryToken;

function login({ accessToken, accessTokenExpiry }, noRedirect, redirectTo) {
  console.log("************login", { accessToken, accessTokenExpiry });
  setAccessToken({
    token: accessToken,
    expiry: accessTokenExpiry,
  });
  if (!noRedirect) {
    Router.push(redirectTo || "/");
  }
}

async function logout() {
  // inMemoryToken = null;
  setAccessToken("");
  const url = "/api/auth/logout";
  const response = await fetch(url, {
    method: "POST",
    credentials: "include",
  });

  // to support logging out from all windows
  window.localStorage.setItem("logout", Date.now());
  Router.push("/login");
}

// Gets the display name of a JSX component for dev tools
const getDisplayName = (Component) =>
  Component.displayName || Component.name || "Component";

const subMinutes = function (dt, minutes) {
  return new Date(dt.getTime() - minutes * 60000);
};

function withAuthSync(WrappedComponent) {
  console.log("************withAuthSync", new Date().getTime());

  return class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`;

    static async getInitialProps(ctx) {
      console.log("************getInitialProps", new Date().getTime());
      console.log("inMemoryToken = ", getAccessToken());

      const token = await auth(ctx);
      if (!getAccessToken()) {
        setAccessToken(token);
      }

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx));

      return {
        ...componentProps,
        accessToken: getAccessToken(),
        isServer: !!ctx.req,
      };
    }

    constructor(props) {
      console.log("************constructor", new Date().getTime());
      super(props);
      this.syncLogout = this.syncLogout.bind(this);
    }

    async componentDidMount() {
      console.log(
        "************componentDidMount",
        this.props,
        new Date().getTime()
      );
      if (!this.props.accessToken) {
        console.log("calling /login");

        return Router.push("/login");
      }

      this.interval = setInterval(async () => {
        if (getAccessToken()) {
          if (
            subMinutes(new Date(getAccessToken().expiry), 1) <=
            new Date(getAccessToken().expiry)
          ) {
            getAccessToken(null);
            const token = await auth();
            getAccessToken(token);
          }
        } else {
          const token = await auth();
          getAccessToken(token);
        }
      }, 5000);

      window.addEventListener("storage", this.syncLogout);
    }

    componentWillUnmount() {
      console.log("************componentWillUnmount");
      clearInterval(this.interval);
      window.removeEventListener("storage", this.syncLogout);
      window.localStorage.removeItem("logout");
    }

    syncLogout(event) {
      console.log("************syncLogout");
      if (event.key === "logout") {
        console.log("logged out from storage!");
        Router.push("/login");
      }
    }

    render() {
      console.log("************render", new Date().getTime());
      console.log("inMemoryToken render ===", getAccessToken());

      return <WrappedComponent {...this.props} />;
    }
  };
}

async function auth(ctx) {
  console.log("************auth ctx");
  /*
   * If `ctx.req` is available it means we are on the server.
   * Additionally if there's no token it means the user is not logged in.
   */
  if (!getAccessToken()) {
    console.log("didnt get access token");

    const headers =
      ctx && ctx.req
        ? {
            Cookie: ctx.req.headers.cookie,
          }
        : {};

    const hostname = "http://localhost:8080";
    // const hostname = "https://7confirmations.xyz";

    const url =
      ctx && ctx.req
        ? `${hostname}/api/v1/auth/refresh-token`
        : "/api/v1/auth/refresh-token";

    try {
      const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache",
          ...headers,
        },
        body: JSON.stringify({}),
      });
      if (response.status === 200) {
        let jsonResponse = await response.json();

        console.log("await response.json()====", jsonResponse);

        const {
          accessToken,
          accessTokenExpiry,
          refreshToken,
          refreshTokenExpiry,
        } = jsonResponse;
        // setup httpOnly cookie if SSR
        if (ctx && ctx.req) {
          ctx.res.setHeader(
            "Set-Cookie",
            `refresh_token=${refreshToken};HttpOnly;Max-Age=${refreshTokenExpiry};Path="/"`
          );
        }
        await login({ accessToken, accessTokenExpiry }, true);
      } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
    } catch (error) {
      console.log("EEEEE:::", error);
      // if (ctx && ctx.req) {
      //   ctx.res.writeHead(302, { Location: "/" });
      //   ctx.res.end();
      // }
      // Router.push("/login");
      logout();

      // if (ctx && ctx.res) ctx.res.redirect("/login");

      // Router.push("/login");
    }
  }

  const accessToken = getAccessToken();

  // We already checked for server. This should only happen on client.

  console.log("accessToken  = ", accessToken);

  if (!accessToken) {
    // Router.push("/login");
    if (ctx && ctx.res) ctx.res.redirect("/login");

    Router.push("/login");
  }

  return accessToken;
}

function getToken() {
  return getAccessToken();
}

const getCurrentPath = (originalUrl) => {
  if (typeof window === "object") {
    return window.location.host;
  } else {
    return originalUrl;
  }
};

export { login, logout, withAuthSync, auth, getToken, getCurrentPath };
