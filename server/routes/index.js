const authRoutes = require("./api/auth/auth.routes");
const userRoutes = require("./api/user/user.routes");
const surveyRoutes = require("./api/survey/survey.routes");
const productRoutes = require("./api/products/products.routes");
const orderRoutes = require("./api/order/order.routes");

module.exports = {
  authRoutes,
  userRoutes,
  surveyRoutes,
  productRoutes,
  orderRoutes,
};
