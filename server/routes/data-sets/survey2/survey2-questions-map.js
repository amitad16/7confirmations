module.exports = {
  a: [
    // 1
    {
      question_identifier: "great_digestion_days",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "sleep_type",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "sleep_type",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "sleep_type",
        },
      ],
      prev_question_identifier: null,
    },

    // 2
    {
      question_identifier: "sleep_type",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "active_energetic",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "active_energetic",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "active_energetic",
        },
      ],
      prev_question_identifier: "great_digestion_days",
    },

    // 3
    {
      question_identifier: "active_energetic",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "decision_change",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "decision_change",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "decision_change",
        },
      ],
      prev_question_identifier: "sleep_type",
    },

    // 4
    {
      question_identifier: "decision_change",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "friends_feel",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "friends_feel",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "friends_feel",
        },
      ],
      prev_question_identifier: "active_energetic",
    },

    // 4.5
    {
      question_identifier: "friends_feel",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "memory_power",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "memory_power",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "memory_power",
        },
      ],
      prev_question_identifier: "decision_change",
    },

    // 5
    {
      question_identifier: "memory_power",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "body_structure",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "body_structure",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "body_structure",
        },
      ],
      prev_question_identifier: "decision_change",
    },

    // 6
    {
      question_identifier: "body_structure",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "nature_type",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "nature_type",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "nature_type",
        },
      ],
      prev_question_identifier: "memory_power",
    },

    // 7
    {
      question_identifier: "nature_type",
      responses: [
        {
          response_identifier: "vata",
          next_question_identifier: "period_duration",
        },
        {
          response_identifier: "pitta",
          next_question_identifier: "period_duration",
        },
        {
          response_identifier: "kapha",
          next_question_identifier: "period_duration",
        },
      ],
      prev_question_identifier: "body_structure",
    },

    // 8
    {
      question_identifier: "period_duration",
      responses: [
        {
          response_identifier: "2_3_days",
          next_question_identifier: "mestrual_cycle_duration",
        },
        {
          response_identifier: "4_5_days",
          next_question_identifier: "mestrual_cycle_duration",
        },
        {
          response_identifier: "6_7_days",
          next_question_identifier: "mestrual_cycle_duration",
        },
        {
          response_identifier: "7_plus_days",
          next_question_identifier: "mestrual_cycle_duration",
        },
      ],
      prev_question_identifier: "nature_type",
    },

    // 9
    {
      question_identifier: "mestrual_cycle_duration",
      responses: [
        {
          response_identifier: "21_25_days",
          next_question_identifier: "pms_symptoms",
        },
        {
          response_identifier: "26_30_days",
          next_question_identifier: "pms_symptoms",
        },
        {
          response_identifier: "30_35_days",
          next_question_identifier: "pms_symptoms",
        },
        {
          response_identifier: "35_plus_days",
          next_question_identifier: "pms_symptoms",
        },
      ],
      prev_question_identifier: "period_duration",
    },

    // 10
    {
      question_identifier: "pms_symptoms",
      responses: [
        {
          response_identifier: "abdominal_bloating",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "abdominal_pain",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "sore_breasts",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "acne",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "food_cravings",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "constipation",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "diarrhea",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "headaches",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "sensitivity_light_sound",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "fatigue",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "irritability",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "sleep_patterns_changes",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "anxiety",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "depression",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "sadness",
          next_question_identifier: "get_to_know_medium",
        },
        {
          response_identifier: "emotional_outbursts",
          next_question_identifier: "get_to_know_medium",
        },
      ],
      prev_question_identifier: "mestrual_cycle_duration",
    },

    // 11
    {
      question_identifier: "get_to_know_medium",
      responses: [
        {
          response_identifier: "playstore",
          next_question_identifier: "name",
        },
        {
          response_identifier: "whatsapp",
          next_question_identifier: "name",
        },
        {
          response_identifier: "google",
          next_question_identifier: "name",
        },
        {
          response_identifier: "facebook",
          next_question_identifier: "name",
        },
        {
          response_identifier: "youtube",
          next_question_identifier: "name",
        },
        {
          response_identifier: "instagram",
          next_question_identifier: "name",
        },
        {
          response_identifier: "twitter",
          next_question_identifier: "name",
        },
        {
          response_identifier: "website",
          next_question_identifier: "name",
        },
        {
          response_identifier: "linkedin",
          next_question_identifier: "name",
        },
        {
          response_identifier: "newspaper",
          next_question_identifier: "name",
        },
        {
          response_identifier: "friend",
          next_question_identifier: "name",
        },
      ],
      prev_question_identifier: "pms_symptoms",
    },

    // 12
    {
      question_identifier: "name",
      responses: [],
      prev_question_identifier: "get_to_know_medium",
      next_question_identifier: "whatsapp",
    },

    // 13
    {
      question_identifier: "whatsapp",
      responses: [],
      prev_question_identifier: "email",
      next_question_identifier: "goodbye",
    },

    // 14
    {
      question_identifier: "goodbye",
      responses: [],
      prev_question_identifier: "whatsapp",
      next_question_identifier: null,
    },
  ],
};
