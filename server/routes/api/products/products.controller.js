const boom = require("@hapi/boom");
const Products = require("../../../models/products");
const Cart = require("../../../models/cart");

const getAllProducts = async (req, res) => {
  console.log("req.user.id ==", req.user);

  let products = await Products.find({
    $or: [{ productGroupItem: "1" }, { productGroup: { $exists: false } }],
  }).lean(true);

  let cartProducts = [];
  if (req.user) {
    let cart = await Cart.findOne({ user: req.user.id }, { products: 1 }).lean(
      true
    );

    cartProducts = (cart && cart.products) || cartProducts;
  }
  cartProducts = cartProducts.map((v) => v.toString());

  console.log("cart products = ", cartProducts);

  products = products.map((v) => {
    if (cartProducts.includes(v._id.toString())) v.addedToCart = true;
    return v;
  });

  if (!products) throw boom.notFound("Products not found");

  res.status(200).send(products);
};

const getProductByGroupCategory = async (req, res) => {
  console.log("req.user.id ==", req.user);
  let { groupId, type } = req.params;

  let product = await Products.findOne({
    productGroup: groupId,
    productType: type,
  }).lean(true);

  let cartProducts = [];
  if (req.user) {
    let cart = await Cart.findOne({ user: req.user.id }, { products: 1 }).lean(
      true
    );

    cartProducts = (cart && cart.products) || cartProducts;
  }
  cartProducts = cartProducts.map((v) => v.toString());

  console.log("cart products = ", cartProducts);

  if (cartProducts.includes(product._id.toString())) product.addedToCart = true;

  if (!product) throw boom.notFound("Products not found");

  res.status(200).send(product);
};

const getListedProducts = async (req, res) => {
  let { productIds } = req.body;

  let products = await Products.find({ _id: { $in: productIds } }).lean(true);

  if (!products) throw boom.notFound("Products not found");

  products = products.map((v) => {
    v.addedToCart = true;
    return v;
  });

  res.status(200).send(products);
};

const getCartProducts = async (req, res) => {
  console.log("req.user = ", req.user);

  if (!req.user) throw boom.notAcceptable("User not loggedin");

  let cartProducts = await Cart.findOne({ user: req.user.id })
    .populate("products")
    .lean(true);

  res.status(200).send(cartProducts);
};

const addProductToCart = async (req, res) => {
  let { products } = req.body;

  console.log("req.user = ", req.user);

  if (!req.user) throw boom.notAcceptable("User not loggedin");

  let updatedCart = await Cart.findOneAndUpdate(
    { user: req.user.id },
    { $addToSet: { products: { $each: products } } },
    { upsert: true, new: true }
  ).lean(true);

  res.status(200).send(updatedCart);
};

const removeProductFromCart = async (req, res) => {
  let { productId } = req.params;

  console.log("req.user = ", req.user);

  if (!req.user) throw boom.notAcceptable("User not loggedin");

  let updatedCart = await Cart.findOneAndUpdate(
    { user: req.user.id },
    { $pull: { products: productId } },
    { upsert: true, new: true }
  ).lean(true);

  res.status(200).send(updatedCart);
};

module.exports = {
  getAllProducts,
  getProductByGroupCategory,
  getListedProducts,
  getCartProducts,
  addProductToCart,
  removeProductFromCart,
};
