const express = require("express");
const router = express.Router();
const { asyncMiddleware, validateAuthToken } = require("../../../middlewares");

// Controllers
const {
  getAllProducts,
  getProductByGroupCategory,
  getListedProducts,
  getCartProducts,
  addProductToCart,
  removeProductFromCart,
} = require("./products.controller");

/**
 * Get All Products
 *
 * @method GET
 * @endpoint /api/v1/products
 */
router.get(
  "/",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(getAllProducts)
);

/**
 * Get Product by group category
 *
 * @method GET
 * @endpoint /api/v1/products/group/:groupId/type/:type
 */
router.get(
  "/group/:groupId/type/:type",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(getProductByGroupCategory)
);

/**
 * Get Listed Products
 *
 * @method POST
 * @endpoint /api/v1/products/listed
 */
router.post("/listed", asyncMiddleware(getListedProducts));

/**
 * Get cart Products
 *
 * @method GET
 * @endpoint /api/v1/products/cart
 */
router.get(
  "/cart",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(getCartProducts)
);

/**
 * Add Product to cart for user
 *
 * @method POST
 * @endpoint /api/v1/products/cart/add
 */
router.post(
  "/cart/add",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(addProductToCart)
);

/**
 * Remove Product to cart for user
 *
 * @method POST
 * @endpoint /api/v1/products/:productId/cart/remove
 */
router.post(
  "/:productId/cart/remove",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(removeProductFromCart)
);

module.exports = router;
