const jwtUtil = require("../../../utils/jwt.util");

module.exports = function (data) {
  const { accessToken, accessTokenExpiry } = jwtUtil.generateAccessToken(data);
  const { refreshToken, refreshTokenExpiry } = jwtUtil.generateRefreshToken();

  return { accessToken, accessTokenExpiry, refreshToken, refreshTokenExpiry };
};
