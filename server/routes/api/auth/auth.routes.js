const express = require("express");
const router = express.Router();
const { asyncMiddleware } = require("../../../middlewares");

// Controllers
const {
  registerUser,
  loginUser,
  getNewRefreshToken,
  logoutUser,
  delay,
} = require("./auth.controller");

/**
 * Register new user
 *
 * @method POST
 * @endpoint /api/v1/auth/register
 */
router.post("/register", asyncMiddleware(registerUser));

/**
 * Login user
 *
 * @method POST
 * @endpoint /api/v1/auth/login
 */
router.post("/login", asyncMiddleware(loginUser));

/**
 * get new refresh token
 *
 * @method POST
 * @endpoint /api/v1/auth/refresh-token
 */
router.post("/refresh-token", asyncMiddleware(getNewRefreshToken));

/**
 * Logout user
 *
 * @method DELETE
 * @endpoint /api/v1/auth/logout
 */
router.delete("/logout", asyncMiddleware(logoutUser));

/**
 * Logout user
 *
 * @method GET
 * @endpoint /api/v1/auth/delay
 */
router.get("/delay", asyncMiddleware(delay));

module.exports = router;
