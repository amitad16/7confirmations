const Joi = require("@hapi/joi");

const userSchema = Joi.object({
  fullname: Joi.string().min(3).max(80).required(),
  email: Joi.string().email({ minDomainSegments: 2 }).required(),
  password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")).required(),
});

module.exports = { userSchema };
