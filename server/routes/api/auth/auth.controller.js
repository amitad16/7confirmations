const boom = require("@hapi/boom");

const createAccessAndRefreshToken = require("./createAccessAndRefreshToken.action");

// Modles
const User = require("../../../models/user");
const RefreshToken = require("../../../models/refresh_token");

const { userSchema } = require("./helper");

const registerUser = async (req, res) => {
  let userDetails = ({ fullname, email, password } = req.body);

  console.log("register usreDetails:::", userDetails);

  const { error, value } = userSchema.validate(userDetails);

  if (error) {
    console.log(error.details[0].message, typeof error.details[0].message);

    throw boom.badData(error.details[0].message);
  }

  // Check if user with fullname or email exists
  console.log("user = ", await User.findOne({ email }));

  if (await User.findOne({ email }))
    throw boom.badData(`Email ${email} already exists`);

  let newUser = new User(value);

  let savedUser = await newUser.save();

  if (!savedUser) throw boom.notImplemented("Sorry, error on our side");

  delete value.password;

  const userData = { id: savedUser._id, ...value };

  let {
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
  } = createAccessAndRefreshToken(userData);

  await RefreshToken.findOneAndUpdate(
    { refreshToken },
    {
      $set: {
        refreshToken,
        expiresAt: new Date(new Date().getTime() + refreshTokenExpiry * 1000),
        userId: userData.id,
      },
    },
    { upsert: true }
  );

  res.cookie("refresh_token", refreshToken, {
    maxAge: refreshTokenExpiry * 1000, // convert from seconds to milliseconds
    httpOnly: true,
    secure: false,
  });

  res.status(201).json({
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
    user: userData,
  });
};

const loginUser = async (req, res) => {
  let { email, password } = req.body;

  console.log("login req = ", { email, password });

  if (!email || !password) throw boom.badData("Invalid email or password");

  let user = await User.findOne(
    { email },
    { _id: 1, email: 1, fullname: 1, password: 1 }
  );

  if (!user) throw boom.badRequest("Invalid email or password");

  let passwordMatch = await user.passwordMatch(password);

  if (!passwordMatch) throw boom.badRequest("Invalid email or password");

  const userData = {
    id: user._id,
    fullname: user.fullname,
    email: user.email,
  };

  let {
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
  } = createAccessAndRefreshToken(userData);

  await RefreshToken.findOneAndUpdate(
    { refreshToken },
    {
      $set: {
        refreshToken,
        expiresAt: new Date(new Date().getTime() + refreshTokenExpiry * 1000),
        userId: userData.id,
      },
    },
    { upsert: true }
  );

  res.cookie("refresh_token", refreshToken, {
    maxAge: refreshTokenExpiry * 1000, // convert from seconds to milliseconds
    httpOnly: true,
    secure: false,
  });

  res.status(200).json({
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
    user: userData,
  });
};

const getNewRefreshToken = async (req, res) => {
  let oldRefreshToken = req.cookies["refresh_token"];

  console.log("oldRefreshToken = ", oldRefreshToken);

  if (!oldRefreshToken) return res.sendStatus(401);

  const refreshTokenUserData = await RefreshToken.findOne(
    { refreshToken: oldRefreshToken },
    { userId: 1 }
  ).lean(true);

  if (!refreshTokenUserData || !refreshTokenUserData.userId)
    throw boom.unauthorized("You are not authorized");

  const user = await User.findById(refreshTokenUserData.userId, {
    _id: 1,
    email: 1,
    fullname: 1,
  });

  const userData = {
    id: user._id,
    fullname: user.fullname,
    email: user.email,
  };

  let {
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
  } = createAccessAndRefreshToken(userData);

  await RefreshToken.findOneAndUpdate(
    { refreshToken: oldRefreshToken },
    {
      $set: {
        refreshToken,
        expiresAt: new Date(new Date().getTime() + refreshTokenExpiry * 1000),
        userId: userData.id,
      },
    },
    { upsert: true }
  );

  res.cookie("refresh_token", refreshToken, {
    maxAge: refreshTokenExpiry * 1000, // convert from seconds to milliseconds
    httpOnly: true,
    secure: false,
  });

  res.status(200).json({
    accessToken,
    accessTokenExpiry,
    refreshToken,
    refreshTokenExpiry,
    user: userData,
  });
};

const logoutUser = async (req, res) => {
  let refreshToken = req.cookies["refresh_token"];

  await RefreshToken.findOneAndDelete({ refreshToken });

  res.cookie("refresh_token", "", {
    httpOnly: true,
    expires: new Date(0),
  });

  res.sendStatus(200);
};

const delay = async (req, res) => {
  setTimeout(() => {
    res.sendStatus(200);
  }, 5000);
};

module.exports = {
  registerUser,
  delay,
  loginUser,
  getNewRefreshToken,
  logoutUser,
};
