const boom = require("@hapi/boom");
const uuidv4 = require("uuid").v4;
const Joi = require("@hapi/joi");

const emailService = require("../../../service/email/email.service");
const orderPlacedTemplate = require("../../../service/email/orderPlacedTemplate");

// Modles
const Cart = require("../../../models/cart");
const Order = require("../../../models/orders");

// Libs
const checksumLib = require("../../../service/paytm/checksum");
const { paytm_config } = require("../../../service/paytm/paytm_config");

const deliveryAddressSchema = Joi.object({
  fullname: Joi.string().required(),
  email: Joi.string().email({ minDomainSegments: 2 }).required(),
  mobile: Joi.string().required().length(10),
  address: Joi.string().required(),
  pin: Joi.string().required().length(6),
  state: Joi.string().required(),
  country: Joi.string().equal("India", "india"),
});

const createNewOrder = async (req, res) => {
  if (!req.user) throw boom.forbidden("You are not logged in");

  let { transactionAmount, channelId, products, deliveryAddress } = req.body;

  const { error, value } = deliveryAddressSchema.validate(deliveryAddress);

  if (error) throw boom.badData("Invalid address details");

  deliveryAddress = value;

  let orderData = {
    orderId: uuidv4(),
    customerId: req.user.id,
    products,
    transactionAmount,
    channelId,
    orderStatus: "INITIATED",
    deliveryAddress,
  };

  let newOrder = new Order(orderData);

  await newOrder.save();

  let paytmParams = {
    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    MID: paytm_config.MID,

    /* Find your WEBSITE in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    WEBSITE: paytm_config.WEBSITE,

    /* Find your INDUSTRY_TYPE_ID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    INDUSTRY_TYPE_ID: paytm_config.INDUSTRY_TYPE_ID,

    /* WEB for website and WAP for Mobile-websites or App */
    CHANNEL_ID: channelId,

    /* Enter your unique order id */
    ORDER_ID: orderData.orderId,

    /* unique id that belongs to your customer */
    CUST_ID: orderData.customerId,

    /* customer's mobile number */
    // "MOBILE_NO" : "CUSTOMER_MOBILE_NUMBER",

    /* customer's email */
    EMAIL: req.user.email,

    /**
     * Amount in INR that is payble by customer
     * this should be numeric with optionally having two decimal points
     */
    TXN_AMOUNT: String(orderData.transactionAmount),

    /* on completion of transaction, we will send you the response on this URL */
    // CALLBACK_URL: "http://localhost:8080/api/v1/order/complete",
    CALLBACK_URL: `${process.env.BASE_URL}/api/${process.env.API_VERSION}/order/complete`,
  };

  /**
   * Generate checksum for parameters we have
   * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
   */
  checksumLib.genchecksum(
    paytmParams,
    paytm_config.MERCHANT_KEY,
    async function (err, checksum) {
      /* for Staging */
      let txn_url =
        process.env.NODE_ENV === "production"
          ? "https://securegw.paytm.in/order/process"
          : "https://securegw-stage.paytm.in/order/process";

      /* Prepare HTML Form and Submit to Paytm */
      var form_fields = "";
      for (var x in paytmParams) {
        form_fields +=
          "<input type='hidden' name='" +
          x +
          "' value='" +
          paytmParams[x] +
          "' >";
      }
      form_fields +=
        "<input type='hidden' name='CHECKSUMHASH' value='" + checksum + "' >";

      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(
        '<html><head><title>Merchant Checkout Page</title></head><body><center><h1>Please do not refresh this page...</h1></center><form method="post" action="' +
          txn_url +
          '" name="f1">' +
          form_fields +
          '</form><script type="text/javascript">document.f1.submit();</script></body></html>'
      );
      res.end();
    }
  );
};

const orderComplete = async (req, res) => {
  console.log("req.body", req.body);
  let received_data = req.body;

  var paytmChecksum = "";
  let orderId = received_data["ORDERID"];
  let transactionStatus =
    received_data["STATUS"] === "TXN_SUCCESS" ? "SUCCESS" : "FAIL";

  /**
   * Create an Object from the parameters received in POST
   * received_data should contains all data received in POST
   */
  var paytmParams = {};
  for (var key in received_data) {
    if (key == "CHECKSUMHASH") {
      paytmChecksum = received_data[key];
    } else {
      paytmParams[key] = received_data[key];
    }
  }

  /**
   * Verify checksum
   * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
   */
  var isValidChecksum = checksumLib.verifychecksum(
    paytmParams,
    paytm_config.MERCHANT_KEY,
    paytmChecksum
  );
  if (isValidChecksum) {
    console.log("Checksum Matched");

    let updatedOrder = await Order.findOneAndUpdate(
      { orderId },
      {
        $set: {
          orderStatus: transactionStatus,
          transactionDetails: received_data,
        },
      },
      { new: true }
    )
      .populate("products")
      .lean();

    if (!updatedOrder)
      throw boom.notImplemented("Something is wrong on our side.");

    await Cart.findOneAndDelete({ user: updatedOrder.customerId });

    emailService.sendMail({
      to: process.env.NODEMAILER_EMAIL,
      subject: "New Order Placed",
      html: orderPlacedTemplate(
        updatedOrder.orderId,
        updatedOrder.products,
        updatedOrder.transactionAmount,
        updatedOrder.orderStatus,
        updatedOrder.deliveryAddress
      ),
    });

    res.updatedOrder = updatedOrder;

    res.redirect(302, `/ordercomplete?orderId=${orderId}&orderStatus=success`);
  } else {
    console.log("Checksum Mismatched");

    // res.send("Transaction unsuccessfull");

    res.redirect(302, `/ordercomplete?orderId=${orderId}&orderStatus=fail`);
  }
};

const getOrderStatus = async (req, res) => {
  let { orderId } = req.params;

  let order = await Order.findOne(
    { orderId },
    {
      orderId: 1,
      customerId: 1,
      orderStatus: 1,
      transactionDetails: 1,
    }
  ).lean(true);

  if (!order) throw boom.notFound("Order not found");

  console.log("order = ", order);

  res.status(200).json(order);
};

module.exports = { createNewOrder, orderComplete, getOrderStatus };
