const express = require("express");
const router = express.Router();
const { asyncMiddleware, validateAuthToken } = require("../../../middlewares");

// Controllers
const {
  createNewOrder,
  orderComplete,
  getOrderStatus,
} = require("./order.controller");

/**
 * Create new order
 *
 * @method POST
 * @endpoint /api/v1/order/new
 */
router.post(
  "/new",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(createNewOrder)
);

/**
 * Transaction success callback url
 *
 * @method POST
 * @endpoint /api/v1/order/complete
 */
router.post("/complete", asyncMiddleware(orderComplete));

/**
 * Get order status for user
 *
 * @method GET
 * @endpoint /api/v1/order/:orderId/status
 */
router.get(
  "/:orderId/status",
  asyncMiddleware(validateAuthToken),
  asyncMiddleware(getOrderStatus)
);

module.exports = router;
