const express = require("express");
const router = express.Router();
const { asyncMiddleware } = require("../../../middlewares");

// Controllers
const { getUserResult } = require("./user.controller");

/**
 * Get User result
 *
 * @method GET
 * @endpoint /api/v1/user/:userId/result
 */
router.get("/:userId/result", asyncMiddleware(getUserResult));

module.exports = router;
