const boom = require("@hapi/boom");
const UserResponse = require("../../../models/user_response");

let results = {
  vata: {
    natureType: "You are Vata predominantly (light and quick)",
    natureExplaination:
      "As per Ayurvedic science, this metabolic energy or nature (called dosha in Ayurveda) is unique to each person - you are wired slightly different from everybody else!",
    upsAndDowns: {
      ups: [
        { id: "1", value: "You are Active" },
        { id: "2", value: "Creative and Experimentative" },
        { id: "3", value: "Free and Unobstructed in Thought and Action" },
      ],
      downs: [
        {
          id: "1",
          value: "You could be prone to hyperactivity, anxiety and insomnia",
        },
        { id: "2", value: "Get tired quickly" },
        {
          id: "3",
          value:
            "Since you are generally a quick, light and often a picky eater, you may be prone to vitamin and mineral deficiencies!",
        },
      ],
    },
    healthMantra:
      "To relax, observe and stay in each moment - slow down a bit!",
    traits: [
      { id: "1", value: "You are creative and spontaneous" },
      { id: "2", value: "Your hands and feet are almost always cold to touch" },
      {
        id: "3",
        value: "You lose weight easily and have a tendency to be scrawny",
      },
      { id: "4", value: "You walk fast and talk fast" },
      { id: "5", value: "You toss and turn at night" },
      {
        id: "6",
        value:
          "You are restless and find it difficult to focus on one thing at a time",
      },
      {
        id: "7",
        value: "You keep busy so that you don't overthink or get bored",
      },
      {
        id: "8",
        value:
          "You tend to be self-critical and really appreciate it when you're allowed to express yourself",
      },
      {
        id: "9",
        value:
          "You take a long time to make a decision and still feel unsure after making it",
      },
      {
        id: "10",
        value:
          "You are prone to pain, anxiety, constipation, colon-related disorders, and depression",
      },
    ],
  },

  pitta: {
    natureType: "You are predominantly pitta (intense)",
    natureExplaination:
      "As per Ayurvedic science, this metabolic energy or nature (called dosha in Ayurveda) is unique to each person - you are wired slightly different from everybody else!",
    upsAndDowns: {
      ups: [
        {
          id: "1",
          value: "You are energetic and focussed in thought and action.",
        },
      ],
      downs: [
        {
          id: "1",
          value:
            "Since you are pushing the envelope so often, your immune system may be over-worked. This may lead to inflammation and infections.",
        },
        {
          id: "2",
          value:
            "Your mind and body are great at digesting stuff, but absorption and circulation might be less than ideal. This might lead to acidity, indigestion and heartburn.",
        },
        {
          id: "3",
          value:
            "You are also prone to getting stressed out or burned out, especially if your sleep cycles are affected.",
        },
      ],
    },
    healthMantra:
      "To let go of control sometimes, and let events flow and unfold instead!",
    traits: [
      {
        id: "1",
        value:
          "You are organized, passionate, analytical, and have a good memory",
      },
      {
        id: "2",
        value:
          "You never feel like you have enough time and are always trying not to waste time",
      },
      {
        id: "3",
        value: "You are very active and easily turn red in the sun",
      },
      { id: "4", value: "Your hands usually feel warm to touch" },
      { id: "5", value: "You gain and lose weight easily" },
      {
        id: "6",
        value:
          "You're always hungry and get hangry if you don't get to eat on time",
      },
      {
        id: "7",
        value:
          "You like having things under control and get upset when things don't go your way",
      },
      {
        id: "8",
        value: "You are a list-maker, a planner",
      },
      {
        id: "9",
        value: "You make decisions easily and stick to them",
      },
      {
        id: "10",
        value:
          "You are prone to fever, diarrhea, skin infections, heartburn, inflammation, and hypertension",
      },
    ],
  },

  kapha: {
    natureType: "You are predominantly kapha (slow and steady)",
    natureExplaination:
      "As per Ayurvedic science, this metabolic energy or nature (called dosha in Ayurveda) is unique to each person - you are wired slightly different from everybody else!",
    upsAndDowns: {
      ups: [
        { id: "1", value: "You are calm, composed" },
        { id: "2", value: "You are measured in thought and action" },
      ],
      downs: [
        {
          id: "1",
          value:
            "As someone who doesn't like to change the status quo unless absolutely necessary, you may have a tendency to get lazy, lethargic or depressed.",
        },
        {
          id: "2",
          value:
            "Your mind and body are great at absorbing and retaining stuff, but circulation and digestion could be low-key. This can lead to weight gain, buildup of toxins or clogging of essential bio-chemical pathways, further leading to increasing hormonal insensitivity and imbalance.",
        },
      ],
    },
    healthMantra:
      "To get up and about as often as you can. Don't procrastinate!",
    traits: [
      { id: "1", value: "You are calm, easygoing, and caring" },
      { id: "2", value: "You like doing things at your own pace" },
      {
        id: "3",
        value: "You learn through repetition and have a good long-term memory",
      },
      {
        id: "4",
        value: "You gain weight easily but find it difficult to lose weight",
      },
      { id: "5", value: "You're okay with skipping meals" },
      {
        id: "6",
        value:
          "You like the idea of adventure but you need a big push to follow through with it (your stamina is great though)",
      },
      {
        id: "7",
        value:
          "You tend to carry the weight of other people's emotions and problems",
      },
      {
        id: "8",
        value: "You don't like to plan and prefer to follow others",
      },
      {
        id: "9",
        value: "You make slow, calculated decisions",
      },
      {
        id: "10",
        value:
          "You are prone to colds and coughs, allergies, congestion, sinus headaches, weight gain, and water retention",
      },
    ],
  },
};

const getUserResult = async (req, res) => {
  let userResponse = await UserResponse.findOne(
    { user_id: req.params.userId },
    { profile_responses: 1 }
  ).lean(true);

  if (!userResponse) throw boom.notFound("User Result not found");

  let { profile_responses } = userResponse;

  if (profile_responses) {
    let responseCount = {
      vata: 0,
      pitta: 0,
      kapha: 0,
    };

    let maxResponse = null;

    let answersArr = Object.values(profile_responses);

    console.log("answersArr", answersArr);

    answersArr.forEach((v) => {
      if (v === "vata") responseCount.vata = responseCount.vata + 1;
      else if (v === "pitta") responseCount.pitta = responseCount.pitta + 1;
      else if (v === "kapha") responseCount.kapha = responseCount.kapha + 1;
    });

    maxResponse = Math.max(...Object.values(responseCount));

    let resultType = null;

    for (let i in responseCount) {
      if (responseCount[i] === maxResponse) resultType = i;
    }

    console.log("responsecoutLLL", responseCount, maxResponse, resultType);

    res.status(200).send(results[resultType]);
  }
};

module.exports = { getUserResult };
