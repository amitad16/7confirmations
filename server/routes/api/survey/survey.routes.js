const express = require("express");
const router = express.Router();
const { asyncMiddleware } = require("../../../middlewares");

// Controllers
const {
  initializeSurvey,
  setUserResponse,
  getPrevStep,
  completedSurvey,
} = require("./survey.controller");

/**
 * Initialize survey
 *
 * @method GET
 * @endpoint /api/v1/survey/:surveyId/initialize
 */
router.get("/:surveyId/initialize", asyncMiddleware(initializeSurvey));

/**
 * Set user response and get next step
 *
 * @method POST
 * @endpoint /api/v1/survey/:surveyId/next
 */
router.post("/:surveyId/next", asyncMiddleware(setUserResponse));

/**
 * rollback user response and get prev step
 *
 * @method POST
 * @endpoint /api/v1/survey/:surveyId/prev
 */
router.post("/:surveyId/prev", asyncMiddleware(getPrevStep));

/**
 * Completed survey
 *
 * @method POST
 * @endpoint /api/v1/survey/:surveyId/complete
 */
router.post("/:surveyId/complete", asyncMiddleware(completedSurvey));

module.exports = router;
