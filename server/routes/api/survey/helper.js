const uuid = require("uuid");

const UserResponse = require("../../../models/user_response");

const surveys = require("../../data-sets/survey2/survey2-questions");
const surveyStepsMapping = require("../../data-sets/survey2/survey2-questions-map");

function getSurveyById(surveyId) {
  return surveys[surveyId];
}

async function initializeSurvey(surveyId) {
  let firstStep = getFirstStepOfSurvey(surveyId);

  console.log("firstStep::", firstStep);

  let userId = uuid.v4();

  let newUserResponse = new UserResponse({ user_id: userId });

  await newUserResponse.save();

  return makeResponseStep(surveyId, userId, firstStep);
}

async function getPreviousStep(surveyId, userId) {
  let profileData = await getUserProfileData(userId);

  // console.log("profile data::::", profileData);

  let prevQuestionIdentifier = profileData.steps_completed.pop();

  if (!prevQuestionIdentifier) {
    let firstStep = getFirstStepOfSurvey(surveyId);
    return makeResponseStep(surveyId, userId, firstStep);
  }

  delete profileData.profile_responses[prevQuestionIdentifier];

  await setUserProfileData(userId, profileData);

  let question = findQuestionByQuestionIdentifier(
    surveyId,
    prevQuestionIdentifier
  );

  return makeResponseStep(surveyId, userId, question);
}

async function postQuestionResponse(surveyId, userId, response) {
  let { question_identifier, user_response } = response;

  let profileData = await getUserProfileData(userId);

  // console.log("profile data:::", profileData);

  profileData.profile_responses[question_identifier] = user_response;

  profileData.steps_completed.push(question_identifier);

  await setUserProfileData(userId, profileData);

  console.log("surveyId:::", surveyId);

  return getNextStep(surveyId, userId, response);
}

function getNextStep(surveyId, userId, response) {
  let nextQuestionIdentifier = getNextQuestionIdentifierFromCurrentResponse(
    surveyId,
    response
  );

  console.log("next question identifier::", nextQuestionIdentifier);

  if (!nextQuestionIdentifier) {
    return makeResponseStep(surveyId, userId, { identifier: "__end__" }, true);
  } else {
    let question = findQuestionByQuestionIdentifier(
      surveyId,
      nextQuestionIdentifier
    );

    return makeResponseStep(surveyId, userId, question);
  }
}

function getNextQuestionIdentifierFromCurrentResponse(surveyId, response) {
  let { question_identifier, user_response } = response;

  let response_identifier = Array.isArray(user_response)
    ? user_response[0]
    : user_response;

  let mappedSteps = surveyStepsMapping[surveyId];

  let q = mappedSteps.find((v) => {
    if (v.question_identifier === question_identifier) {
      return v;
    }
  });

  let nextQuestionIdentifier = null;

  if (q.responses && q.responses.length) {
    let a = q.responses.find((r) => {
      if (r.response_identifier === response_identifier) {
        // console.log("rrr::", r);
        return r.next_question_identifier;
      }
    });

    if (a && a.next_question_identifier)
      nextQuestionIdentifier = a.next_question_identifier;
  } else {
    nextQuestionIdentifier = q.next_question_identifier;
  }

  // console.log("nqi:::", nextQuestionIdentifier);

  // if (nextQuestionIdentifier && nextQuestionIdentifier.next_question_identifier)

  return nextQuestionIdentifier;
}

function findQuestionByQuestionIdentifier(surveyId, questionIdentifier) {
  let survey = getSurveyById(surveyId);

  return survey.steps.find((v) => v.identifier === questionIdentifier);
}

async function makeResponseStep(surveyId, userId, step, isLastStep) {
  let totalSteps = getTotalStepsOfSurvey(surveyId);

  //   let profile_data = { profile_responses: {}, steps_completed: [] };

  console.log("userId::::", userId);

  let profile_data = await getUserProfileData(userId);

  // console.log("profile_data;::", profile_data);

  let { profile_responses = {}, steps_completed = [] } = profile_data;

  let stepsCompletedCount = steps_completed.length;

  return {
    attributes: step,
    is_last_step: !!isLastStep,
    profile_responses: profile_responses,
    percent_complete: (stepsCompletedCount / totalSteps) * 100,
    total_steps: totalSteps,
    current_step: parseInt(stepsCompletedCount + 1, 10),
    clientId: userId,
  };
}

function getTotalStepsOfSurvey(surveyId) {
  return surveys[surveyId].steps.length;
}

function getFirstStepOfSurvey(surveyId) {
  let survey = getSurveyById(surveyId);

  let { steps } = survey;

  if (steps && steps.length) {
    return steps.find((v) => !v.prev_step_identifier);
  }
}

async function getUserProfileData(userId) {
  let response = await UserResponse.findOne(
    { user_id: userId },
    { profile_responses: 1, steps_completed: 1 }
  ).lean(true);

  // console.log("response;:", response);
  // console.log("full response;::", { ...response });

  if (!response.profile_responses) response.profile_responses = {};

  return response;
}

async function setUserProfileData(userId, userResponse) {
  // console.log("new user response:::", userResponse);

  return await UserResponse.findOneAndUpdate(
    { user_id: userId },
    {
      $set: {
        profile_responses: userResponse.profile_responses,
        steps_completed: userResponse.steps_completed,
      },
    }
  );
}

module.exports = {
  initializeSurvey,
  getPreviousStep,
  postQuestionResponse,
  getUserProfileData,
};
