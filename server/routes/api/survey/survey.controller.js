const helper = require("./helper");
const emailService = require("../../../service/email/email.service");

const surveyCompleteMailTemplate = require("../../../service/email/surveyConpleteTemplate");

const initializeSurvey = async (req, res) => {
  console.log("requestins:::initializeSurvey");

  let initial = await helper.initializeSurvey(req.params.surveyId);

  res.status(200).send(initial);
};

const setUserResponse = async (req, res) => {
  console.log("requesting  setUserResponse ");

  let next = await helper.postQuestionResponse(
    req.params.surveyId,
    req.body.userId,
    req.body.response
  );

  res.status(200).send(next);
};

const getPrevStep = async (req, res) => {
  console.log("requesting::: getPrevStep ");

  let prev = await helper.getPreviousStep(req.params.surveyId, req.body.userId);

  res.status(200).send(prev);
};

const completedSurvey = async (req, res) => {
  console.log("completed survey::");

  let { userId } = req.body;

  let userProfileData = await helper.getUserProfileData(userId);

  let { profile_responses } = userProfileData;

  if (profile_responses) {
    let responseCount = {
      vata: 0,
      pitta: 0,
      kapha: 0,
    };

    let maxResponse = null;

    let answersArr = Object.values(profile_responses);

    answersArr.forEach((v) => {
      if (v === "vata") responseCount.vata = responseCount.vata + 1;
      else if (v === "pitta") responseCount.pitta = responseCount.pitta + 1;
      else if (v === "kapha") responseCount.kapha = responseCount.kapha + 1;
    });

    maxResponse = Math.max(...Object.values(responseCount));

    let resultType = null;

    for (let i in responseCount) {
      if (responseCount[i] === maxResponse) resultType = i;
    }

    emailService.sendMail({
      to: process.env.NODEMAILER_EMAIL,
      subject: "A New User completed survey",
      html: surveyCompleteMailTemplate(
        userId,
        profile_responses.name,
        profile_responses.whatsapp,
        resultType
      ),
    });
  }
};

module.exports = {
  initializeSurvey,
  setUserResponse,
  getPrevStep,
  completedSurvey,
};
