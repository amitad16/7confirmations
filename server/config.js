module.exports = {
  auth: {
    jwt: {
      access: {
        issuer: "7Confirmations",
        expiresIn: "30d",
        expireTime: 30 * 24 * 60 * 60, // in seconds
      },
      refresh: {
        expireTime: 30 * 24 * 60 * 60, // 30 Days (in seconds)
      },
    },
  },
};
