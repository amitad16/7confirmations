const jwt = require("jsonwebtoken");
const uuidv4 = require("uuid").v4;

const config = require("../config");

const accessTokenOptions = {
  issuer: config.auth.jwt.access.issuer,
  expiresIn: config.auth.jwt.access.expiresIn,
};
const refreshTokenOptions = config.auth.jwt.refresh;

function generateAccessToken(data) {
  const accessToken = jwt.sign(
    data,
    process.env.ACCESS_TOKEN_SECRET,
    accessTokenOptions
  );
  const accessTokenExpiry = config.auth.jwt.access.expireTime;

  return { accessToken, accessTokenExpiry };
}

function generateRefreshToken() {
  const refreshToken = uuidv4();
  const refreshTokenExpiry = refreshTokenOptions.expireTime;

  return { refreshToken, refreshTokenExpiry };
}

function verifyAccessToken(token) {
  return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, accessTokenOptions);
}

function verifyRefreshToken(token) {
  // return jwt.verify(
  //   token,
  //   process.env.REFRESH_TOKEN_SECRET,
  //   refreshTokenOptions
  // );
}

module.exports = {
  generateAccessToken,
  generateRefreshToken,
  verifyAccessToken,
  verifyRefreshToken,
};
