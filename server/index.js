const express = require("express");
const next = require("next");

const PORT = process.env.PORT || 8080;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });

const routes = require("./routes");

const handler = routes.getRequestHandler(app);

app
  .prepare()
  .then(() => {
    const server = express();

    require("./startup/db")();
    require("./startup/middlewares").networkMiddleware(server);
    require("./startup/routes")(server);

    // const staticPath = path.join(__dirname, '../static')
    // server.use('/static', express.static(staticPath, {
    //   maxAge: '30d',
    //   immutable: true
    // }))

    server.get("*", (req, res) => handler(req, res));

    server.listen(PORT, (err) => {
      if (err) throw err;
      console.log("> Ready on ", PORT);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
