const nextRoutes = require("next-routes");
const routes = (module.exports = nextRoutes());

const APP_ROUTES = [
  {
    page: "index",
    pattern: "/",
  },
  {
    page: "products",
    pattern: "/products",
  },
];

APP_ROUTES.forEach((route) => routes.add(route));
