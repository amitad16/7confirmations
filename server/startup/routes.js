const { errorMiddleware } = require("../middlewares");

// ROUTES
const {
  authRoutes,
  userRoutes,
  surveyRoutes,
  productRoutes,
  orderRoutes,
} = require("../routes/index");

module.exports = function (app) {
  // Auth Routes
  app.use(`/api/${process.env.API_VERSION}/auth`, authRoutes);

  // User Routes
  app.use(`/api/${process.env.API_VERSION}/user`, userRoutes);

  // Survey Routes
  app.use(`/api/${process.env.API_VERSION}/survey`, surveyRoutes);

  // Product routes
  app.use(`/api/${process.env.API_VERSION}/products`, productRoutes);

  // Order routes
  app.use(`/api/${process.env.API_VERSION}/order`, orderRoutes);

  // All the routes above this line
  app.use(errorMiddleware);
};
