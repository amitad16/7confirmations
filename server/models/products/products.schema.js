const mongoose = require("mongoose");
const { Schema } = mongoose;

const ProductsSchema = new Schema(
  {
    heading: { type: String },
    subHeading: { type: String },
    productImage: { type: String },
    productPrice: { type: String },
    infoList: [{ id: { type: String }, value: { type: String } }],
  },
  {
    timestamps: true,
  }
);

module.exports = ProductsSchema;
