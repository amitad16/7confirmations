const mongoose = require("mongoose");

// Schema
const ProductsSchema = require("./products.schema");

const Products = mongoose.model("products", ProductsSchema, "products");

module.exports = Products;
