const mongoose = require("mongoose");

// Schema
const RefreshTokenSchema = require("./refresh_token.schema");

const RefreshToken = mongoose.model(
  "refresh_tokens",
  RefreshTokenSchema,
  "refresh_tokens"
);

module.exports = RefreshToken;
