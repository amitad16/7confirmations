const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserResponseSchema = new Schema(
  {
    user_id: { type: String, required: true },
    profile_responses: { type: Object, required: true, default: {} },
    steps_completed: [{ type: String }],
  },
  {
    timestamps: true,
  }
);

module.exports = UserResponseSchema;
