const mongoose = require("mongoose");

// Schema
const UserResponseSchema = require("./user_response.schema");

const UserResponse = mongoose.model(
  "user_response",
  UserResponseSchema,
  "user_response"
);

module.exports = UserResponse;
