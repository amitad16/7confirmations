const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require("bcryptjs");

const UserSchema = new Schema(
  {
    fullname: {
      type: String,
      minLength: 3,
      maxlength: 80,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// Encrypt password before save
UserSchema.pre("save", async function (next) {
  let user = this;

  if (user.isModified("password") || user.isNew) {
    let salt = await bcrypt.genSalt(
      parseInt(process.env.BCRYPT_SALT_WORK_FACTOR)
    );

    if (user.password) {
      let hashedPassword = await bcrypt.hash(user.password, salt);
      user.password = hashedPassword;
      next();
    }
  } else {
    next();
  }
});

// Compare pasword
UserSchema.methods.passwordMatch = async function (password) {
  let user = this;

  try {
    let isMatch = await bcrypt.compare(password, user.password);

    return isMatch;
  } catch (err) {
    return false;
  }
};

module.exports = UserSchema;
