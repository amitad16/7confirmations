const mongoose = require("mongoose");

// Schema
const UserSchema = require("./user.schema");

const User = mongoose.model("users", UserSchema, "users");

module.exports = User;
