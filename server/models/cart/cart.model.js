const mongoose = require("mongoose");

// Schema
const CartSchema = require("./cart.schema");

const Cart = mongoose.model("cart", CartSchema, "cart");

module.exports = Cart;
