const mongoose = require("mongoose");
const { Schema } = mongoose;

const CartSchema = new Schema(
  {
    user: { ref: "user", type: Schema.Types.ObjectId, required: true },
    products: [
      { ref: "products", type: Schema.Types.ObjectId, required: true },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = CartSchema;
