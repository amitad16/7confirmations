const mongoose = require("mongoose");

// Schema
const OrdersSchema = require("./orders.schema");

const Orders = mongoose.model("orders", OrdersSchema, "orders");

module.exports = Orders;
