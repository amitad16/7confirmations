const mongoose = require("mongoose");
const { Schema } = mongoose;

const OrdersSchema = new Schema(
  {
    orderId: { type: String, required: true, unique: true },
    customerId: { ref: "user", type: Schema.Types.ObjectId, required: true },
    products: [
      { ref: "products", type: Schema.Types.ObjectId, required: true },
    ],
    transactionAmount: { type: Number, required: true },
    channelId: { type: String, enum: ["WEB", "WAP"], required: true },
    orderStatus: {
      type: String,
      enum: ["INITIATED", "PROCESSING", "SUCCESS", "FAIL"],
    },
    transactionMedium: { type: String, default: "PAYTM", required: true },
    transactionDetails: { type: Object },
    deliveryAddress: { type: Object, required: true },
  },
  {
    timestamps: true,
  }
);

module.exports = OrdersSchema;
