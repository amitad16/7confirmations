const mongoose = require("mongoose");

module.exports = async () => {
  mongoose.Promise = global.Promise;

  console.log("mongo uri = ", process.env.MONGODB_URI);

  await mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
};

// const mongoose = require("mongoose");

// module.exports = async () => {
//   mongoose.Promise = global.Promise;

//   await mongoose.connect(process.env.MONGODB_URI, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   });
//   console.info("Connected to MongoDB...");
// };
