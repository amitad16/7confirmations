module.exports = function (
  orderId,
  products,
  orderTotal,
  orderStatus,
  deliveryAddress
) {
  let _products = `<table><thead>
<tr>
    <th>Product Name</th>
    <th>Product Qty</th>
    <th>Product Price</th>
</tr>
</thead>
<tbody>`;

  for (let product of products) {
    _products += `<tr><td>${product.heading}</td><td>1</td><td>${product.price}</td></tr>`;
  }

  _products += "</tbody></table>";

  return `
        <h2>
            New ORDER Placed.
        </h2>

        <div>
        
            <strong>Order Id: </strong> ${orderId} <br />
            <br />
            <strong>Order Status: </strong> ${orderStatus} <br />
            <br />
            ${_products}
            <br />
            <strong>Order Total: </strong> ${orderTotal} <br /><br />            
            
            <div>
                <strong>Delivery Address: </strong> <br />
                
                Email - ${deliveryAddress.email}<br />
                Phone - ${deliveryAddress.mobile}<br />
                <br />
                ${deliveryAddress.fullname}<br />
                ${deliveryAddress.address}<br />
                PIN - ${deliveryAddress.pin}<br />
                ${deliveryAddress.state}<br />
                ${deliveryAddress.country}<br />
            </div>

        </div>
    `;
};
