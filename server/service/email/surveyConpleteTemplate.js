module.exports = function (userId, userName, userPhone, userResult) {
  return `
        <h2>
            A new user recently took your survey.
        </h2>

        <div>
            <strong>Id: </strong> ${userId} <br />
            <strong>Name: </strong> ${userName} <br />
            <strong>Phone: </strong> ${userPhone} <br />
            <strong>Result: </strong> ${userResult} <br />
        </div>
    `;
};
