const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.NODEMAILER_EMAIL,
    pass: process.env.NODEMAILER_EMAIL_PASS,
  },
});

const defaultMailOptions = {
  from: process.env.NODEMAILER_EMAIL,
  subject: "A Message from 7Confirmations",
};

function sendMail(mailOptions) {
  mailOptions = { ...defaultMailOptions, ...mailOptions };

  console.log("mailOpetions:::", mailOptions);

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}

module.exports = { sendMail };
