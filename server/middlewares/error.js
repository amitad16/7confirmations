/**
 * Handle server error and sends response back to client
 *
 * @param {object} res  express response object
 * @param {number} statusCode status code to send alng with response
 * @param {any} err client message to be send along with response
 */
function sendServerError(res, statusCode, err) {
  if (statusCode < 500) statusCode = 500;

  let errMessage = null;

  if (err.isBoom) {
    errMessage = err.output.payload;
  } else {
    errMessage = "Something went wrong";
  }
  res.status(statusCode).send(errMessage);
}

module.exports = function (err, req, res, next) {
  console.error("err:::", err);

  if (err.isBoom) {
    console.log("boom");
    let { isServer, output } = err;
    let { statusCode, payload } = output;
    if (isServer) return sendServerError(res, statusCode, err);
    res.status(statusCode).send(payload);
  } else {
    console.log("not boom");
    sendServerError(res, 500, err);
  }
};
