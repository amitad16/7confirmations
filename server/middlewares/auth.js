const boom = require("@hapi/boom");
const jwt = require("jsonwebtoken");

const config = require("../config");

module.exports = (req, res, next) => {
  const authorizationHeader = req.headers.authorization;
  const tokenCompulsory = req.headers["x-auth-token-compulsory"];

  console.log("===NOW VALIDATING AUTH TOKEN=====");

  if (
    !authorizationHeader &&
    tokenCompulsory !== undefined &&
    tokenCompulsory === "false"
  )
    return next();

  if (authorizationHeader) {
    const token = req.headers.authorization.split(" ")[1]; // Bearer <token>

    console.log(
      "token ===",
      req.headers.authorization,
      req.headers.authorization.split(" "),
      token
    );

    if (
      !token &&
      tokenCompulsory !== undefined &&
      tokenCompulsory === "false"
    ) {
      return next();
    }

    const options = {
      issuer: config.auth.jwt.access.issuer,
      expiresIn: config.auth.jwt.access.expiresIn,
    };

    try {
      // verify makes sure that the token hasn't expired and has been issued by us
      let result = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, options);

      // Let's pass back the decoded token to the request object
      req.user = result;

      // We call next to pass execution to the subsequent middleware
      console.log("===AUTH TOKEN VALIDATION SUCCESSFUL===");

      next();
    } catch (err) {
      console.log("===AUTH TOKEN VALIDATION UNSUCCESSFUL===", err.message);

      // Throw an error just in case anything goes wrong with verification
      throw boom.unauthorized(err.message);
    }
  } else {
    console.log("===AUTH TOKEN VALIDATION UNSUCCESSFUL 2===");

    throw boom.unauthorized("Authentication error. Token required.");
  }
};
