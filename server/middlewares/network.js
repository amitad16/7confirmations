const bodyParser = require("body-parser");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");

const allowCrossDomain = (req, res, next) => {
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, ccess-Control-Request-Method, Access-Control-Request-Headers, Authorization, x-auth-token-compulsory"
  );

  next();
};

module.exports = (app) => {
  app.use(helmet());

  app.use(compression());

  app.use(cookieParser());

  // Body Parser Middleware
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
};
