import styles from "./ReviewCard.css";

const ReviewCard = ({ userName, productName, review }) => {
  return (
    <div className={styles.ReviewCard}>
      <div className={styles.ReviewCard__title}>{userName}</div>
      <div className={styles.ReviewCard__subtitle}>{productName}</div>
      <div className={styles.ReviewCard__review}>{review}</div>
    </div>
  );
};

export default ReviewCard;
