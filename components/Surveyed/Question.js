import React from "react";

import styles from "./Surveyed.module.css";

const Question = ({ title, description }) => {
  return (
    <div className={styles.Question}>
      <p className={styles.Question__title}>{title}</p>
      <p className={styles.Question__desc}>{description}</p>
    </div>
  );
};

export default Question;
