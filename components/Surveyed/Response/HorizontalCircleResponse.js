import React from "react";

const HorizontalCircleResponse = ({
  // id,
  identifier,
  title,
  description,
  handleResponseClick,
}) => {
  function handleClick() {
    handleResponseClick(identifier);
  }

  return (
    <div
      className="HorizontalCircleResponse"
      style={{
        backgroundColor: "#eee",
        padding: "16px",
        display: "inline-block",
        margin: "4px",
        cursor: "pointer",
      }}
      onClick={handleClick}
    >
      <h4>{title}</h4>
      <small>{description}</small>
    </div>
  );
};

export default HorizontalCircleResponse;
