import React from "react";

import styles from "./PricePlans.css";

import { Grid, Segment } from "semantic-ui-react";

const SinglePricePlan = ({ planName, planPrice, planTimePeriod }) => {
  return (
    <Grid.Column>
      <Segment className={styles.SinglePricePlan}>
        <div className={styles.SinglePricePlan__name}>{planName}</div>
        <div className={styles.SinglePricePlan__price_wrapper}>
          <span className={styles.SinglePricePlan__price}>{planPrice}</span>
          <span className={styles.SinglePricePlan__time_period}>
            / {planTimePeriod}
          </span>
        </div>
      </Segment>
    </Grid.Column>
  );
};

export default SinglePricePlan;
