import React from "react";

import { Grid } from "semantic-ui-react";

import styles from "./PricePlans.css";

import SinglePricePlan from "./SinglePricePlan";

const plans = [
  {
    id: "1",
    name: "Improve Sexual Energy",
    price: "3,500",
    timePeriod: "1 Month",
  },
  {
    id: "2",
    name: "Manage your PCOS",
    price: "3,000",
    timePeriod: "1 Month",
  },
];

const PricePlans = ({}) => {
  return (
    <div className={styles.PricePlans}>
      <Grid container columns={2} stackable>
        {plans.map((plan) => (
          <SinglePricePlan
            key={plan.id}
            planName={plan.name}
            planPrice={plan.price}
            planTimePeriod={plan.timePeriod}
          />
        ))}
      </Grid>
    </div>
  );
};

export default PricePlans;
