import React from "react";

import styles from "./SimpleSection.css";

const Heading = ({ title }) => {
  return <h1 className={`ui header ${styles.Heading}`}>{title}</h1>;
};

export default Heading;
