import React from "react";
import { Container } from "semantic-ui-react";

import styles from "./SimpleSection.css";

import Heading from "./Heading";
import SubHeading from "./SubHeading";

const SimpleSection = ({
  className,
  heading,
  subHeading,
  bgColor,
  children,
  style,
}) => {
  return (
    <div
      className={`ui vertical segment ${styles.SimpleSection} ${
        !!className ? className : ""
      }`}
      style={{ ...style, backgroundColor: bgColor }}
    >
      <div className="ui stackable center aligned page grid">
        <div className="row">
          <div className="wide column">
            {heading && <Heading title={heading} />}
            {subHeading && <SubHeading title={subHeading} />}
            <Container>{children}</Container>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SimpleSection;
