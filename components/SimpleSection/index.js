import SimpleSection from "./SimpleSection";
import Heading from "./Heading";
import SubHeading from "./SubHeading";
import Content from "./Content";

export { Heading, SubHeading, Content };

export default SimpleSection;
