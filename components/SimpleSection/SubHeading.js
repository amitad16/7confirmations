import React from "react";

import styles from "./SimpleSection.css";

const SubHeading = ({ title }) => {
  return <p className={`ui centered lead ${styles.SubHeading}`}>{title}</p>;
};

export default SubHeading;
