import { Form } from "semantic-ui-react";

import styles from "./Card.css";

const productCategoryTypes = [
  {
    name: "GUMMIES",
    value: "Gummies",
  },
  {
    name: "POWDER",
    value: "Powder",
  },
  {
    name: "JUICE",
    value: "Juice",
  },
];

const ProductCard = ({
  _id,
  productGroup,
  productType,
  productName,
  productPrescription,
  productImage,
  productPrice,
  infoList,
  getProductFromGroup,
  addedToCart,
  addToCart,
  removeFromCart,
}) => {
  function handleProductCategoryChange({ target }) {
    console.log("handleProductCategoryChange ==", productGroup, target.value);
    getProductFromGroup(_id, productGroup, target.value);
  }

  function handleAddToCart() {
    addToCart(_id);
  }

  function handleremoveFromCart() {
    removeFromCart(_id);
  }

  return (
    <div className={`${styles.Card} ${styles.ProductCard}`}>
      <div className={styles.ProductCard__image}>
        <img src={`/img/${productImage}`} alt={productName} />
      </div>

      <div className={styles.ProductCard__details}>
        <p className={styles.ProductCard__details__heading}>{productName}</p>
        <p className={styles.ProductCard__details__subheading}>
          {productPrescription}
        </p>
        {productGroup && (
          <div>
            <Form.Field
              className={styles.ProductCard__details__category}
              defaultValue={productType}
              onChange={handleProductCategoryChange}
              control="select"
              label="I will consume it in the form of:"
            >
              {productCategoryTypes.map((v) => (
                <option key={v.name} value={v.name}>
                  {v.value}
                </option>
              ))}
            </Form.Field>
          </div>
        )}
        <ul className={styles.ProductCard__details__info}>
          {infoList &&
            infoList.map((v) => (
              <li key={v.id} className={styles.ProductCard__details__info_item}>
                {v.value}
              </li>
            ))}
        </ul>
        {productGroup && (
          <div>
            <Form.Field
              className={styles.ProductCard__details__category}
              defaultValue={productType}
              onChange={handleProductCategoryChange}
              control="select"
              label="I will consume it in the form of:"
            >
              {productCategoryTypes.map((v) => (
                <option key={v.name} value={v.name}>
                  {v.value}
                </option>
              ))}
            </Form.Field>
          </div>
        )}
      </div>

      <div className={styles.ProductCard__footer}>
        <div className={styles.ProductCard__footer__left}>₹ {productPrice}</div>
        {addedToCart ? (
          <div
            className={styles.ProductCard__footer__right}
            onClick={handleremoveFromCart}
            style={{ color: "#ccc" }}
          >
            Remove
          </div>
        ) : (
          <div
            className={styles.ProductCard__footer__right}
            onClick={handleAddToCart}
          >
            Add
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductCard;
