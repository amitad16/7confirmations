import styles from "./Card.css";

const InfoCard = ({ title, body, bgImage }) => {
  return (
    <div className={styles.InfoCard} style={{ backgroundImage: bgImage }}>
      <p className={styles.InfoCard__title}>{title}</p>
      <p className={styles.InfoCard__body}>{body}</p>
    </div>
  );
};

export default InfoCard;
