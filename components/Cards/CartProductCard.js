import styles from "./Card.css";

const CartProductCard = ({
  _id,
  productType,
  productName,
  productPrescription,
  productImage,
  productPrice,
}) => {
  return (
    <div className={`${styles.CartProductCard}`}>
      <div className={styles.CartProductCard__image}>
        <img src={`/img/${productImage}`} alt={productName} />
      </div>

      <div className={styles.CartProductCard__details}>
        <p className={styles.CartProductCard__details__heading}>
          {productName}
        </p>
        <p className={styles.CartProductCard__details__subheading}>
          {productPrescription}
        </p>

        <div className={styles.CartProductCard__details__price}>
          ₹ {productPrice}
        </div>
      </div>
    </div>
  );
};

export default CartProductCard;
