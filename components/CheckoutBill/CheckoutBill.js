import styles from "./CheckoutBill.css";

const CheckoutBill = ({ subtotal, shipping, total, handleCheckout }) => {
  return (
    <div className={styles.CheckoutBill}>
      <div className={styles.CheckoutBill__calcs}>
        <div className={styles.CheckoutBill__calcs_item}>
          <div className={styles.CheckoutBill__calcs_item_name}>Subtotal</div>
          <div className={styles.CheckoutBill__calcs_item_price}>
            ₹ {subtotal}
          </div>
        </div>

        <div className={styles.CheckoutBill__calcs_item}>
          <div className={styles.CheckoutBill__calcs_item_name}>Shipping</div>
          <div className={styles.CheckoutBill__calcs_item_price}>
            {shipping ? <span>₹ {shipping}</span> : "FREE"}
          </div>
        </div>

        <div className={styles.CheckoutBill__calcs_item}>
          <div className={styles.CheckoutBill__calcs_item_name}>Total</div>
          <div className={styles.CheckoutBill__calcs_item_price}>₹ {total}</div>
        </div>
      </div>

      <div className={styles.CheckoutBill__btn}>
        <button
          className="btn-theme btn-large"
          type="button"
          disabled={!total}
          onClick={handleCheckout}
        >
          Checkout
        </button>
      </div>
    </div>
  );
};

export default CheckoutBill;
