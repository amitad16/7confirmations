import React from "react";
import { Grid, Image } from "semantic-ui-react";

import styles from "./UpsDownDifferences.css";

const UpsDownDifferences = ({ ups, downs }) => {
  return (
    <Grid columns={2} padded>
      <Grid.Column>
        <p
          className={`${styles.UpsDownDifferences__title} ${styles.UpsDownDifferences__title_positive}`}
        >
          Ups
        </p>
        {ups.map((v) => (
          <div key={v.id} className={styles.UpsDownDifferences__item}>
            {v.value}
          </div>
        ))}
      </Grid.Column>
      <Grid.Column>
        <p
          className={`${styles.UpsDownDifferences__title}  ${styles.UpsDownDifferences__title_negative}`}
        >
          Downs
        </p>
        {downs.map((v) => (
          <div key={v.id} className={styles.UpsDownDifferences__item}>
            {v.value}
          </div>
        ))}
      </Grid.Column>
    </Grid>
  );
};

export default UpsDownDifferences;
