import {
  Segment,
  Container,
  Grid,
  Header,
  List,
  Icon,
} from "semantic-ui-react";
import Link from "next/link";

import styles from "./Footer.css";

const Footer = () => {
  return (
    <Segment className={styles.Footer} inverted vertical>
      <Container style={{ textAlign: "center" }}>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={5}>
              <Header
                className={styles.Footer__section_header}
                inverted
                as="h1"
                content="7C"
              />
              <div className={styles.Footer__social_links}>
                <a
                  href="https://wa.me/919625134457?text=Hey! I am interested in 7Confirmations, can I know more about it?"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Icon className={styles.Footer__icon} name="whatsapp" />
                </a>
                <a
                  href="https://www.facebook.com/7confirmations/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Icon className={styles.Footer__icon} name="facebook" />
                </a>
                <a
                  href="https://www.instagram.com/7confirmations/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Icon className={styles.Footer__icon} name="instagram" />
                </a>
              </div>
            </Grid.Column>
            <Grid.Column className={styles.Footer__section} width={5}>
              <Header
                className={styles.Footer__section_header}
                inverted
                as="h4"
                content="Services"
              />
              <List link inverted>
                <List.Item className={styles.Footer__section_item}>
                  <Link href="/products">
                    <a>Products</a>
                  </Link>
                </List.Item>
                <List.Item className={styles.Footer__section_item}>
                  <Link href="/survey/new">
                    <a>Know About You</a>
                  </Link>
                </List.Item>
              </List>
            </Grid.Column>
            <Grid.Column className={styles.Footer__section} width={6}>
              <Header
                className={styles.Footer__section_header}
                inverted
                as="h4"
                content="Contact"
              />
              <List link inverted>
                <List.Item className={styles.Footer__section_item}>
                  {/* <Icon className={styles.Footer__icon} name="phone" /> */}
                  +91-9625134457
                </List.Item>
                <List.Item className={styles.Footer__section_item}>
                  <a href="mailto:7confirmations@gmail.com">
                    {/* <Icon className={styles.Footer__icon} name="mail" /> */}
                    7confirmations@gmail.com
                  </a>
                </List.Item>
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <br />
        <p className={styles.Footer__bottom}>
          {/* <strong> */}
          All rights reserved. 7-Confirmations &copy; {new Date().getFullYear()}
          {/* </strong> */}
        </p>
      </Container>
    </Segment>
  );
};

export default Footer;
