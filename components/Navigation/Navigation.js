import React, { Component } from "react";
import { connect } from "react-redux";
import { Menu, Responsive } from "semantic-ui-react";

import styles from "./Navigation.css";
import DesktopNavigation from "./DesktopNavigation";
import MobileNavigation from "./MobileNavigation";
import { addProductsToCart } from "../../containers/ProductPage/actions";
import { getUserToken } from "../../utils/client/auth.util";

class Navigation extends Component {
  state = {
    windowWidth: 0,
    menuOpen: false,
    isLoggedIn: false,
    // cartItemCount: 0,
  };

  handleCartItemCount(e) {
    let cartItemCount = 0;

    console.log("upaded localshoteag");

    // if (!cartItemCount || cartItemCount === 0) {
    let cart = window.localStorage.getItem("cart");

    if (cart) {
      cart = JSON.parse(cart);

      if (cart) {
        cartItemCount = cart.length;
        // this.props.addProductsToCart(cart);
      }
    }

    this.setState({ cartItemCount });
  }

  componentDidMount() {
    this.handleOnUpdate();

    window.addEventListener("storage", this.handleCartItemCount);

    window.addEventListener("storage", () => {
      // When local storage changes, dump the list to
      // the console.
      console.log(JSON.parse(window.localStorage.getItem("cart")));
    });

    let cartItemCount = 0;

    // if (!cartItemCount || cartItemCount === 0) {
    let cart = window.localStorage.getItem("cart");

    if (cart) {
      cart = JSON.parse(cart);

      if (cart) {
        cartItemCount = cart.length;
        // this.props.addProductsToCart(cart);
      }
    }
    // }

    this.setState({
      isLoggedIn: !!window.localStorage.getItem("__user__"),
      cartItemCount,
    });
  }

  componentWillUnmount() {
    window.removeEventListener("storage", this.handleCartItemCount);
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (this.props.cartItemCount !== prevState.cartItemCount) {
  //     console.log("updated = ", this.props.cartItemCount);
  //     this.setState({ cartItemCount: this.props.cartItemCount });
  //   }
  // }

  handleOnUpdate = () => {
    this.setState({ windowWidth: window.innerWidth });
  };

  setMenuOpen = (menuOpen) => {
    this.setState({ menuOpen });
  };

  handleLogout = () => {
    console.log("logout");
    localStorage.removeItem("__user__");
    window.location.replace("/");
  };

  render() {
    return (
      <Responsive
        as={Menu}
        secondary
        pointing
        fixed={this.state.menuOpen ? "top" : null}
        className={`${styles.Navigation} ${
          !!this.props.className ? this.props.className : ""
        } ${
          this.state.scrollPosition > 0 || this.state.menuOpen ? "bg-white" : ""
        }`}
        onUpdate={this.handleOnUpdate}
      >
        {this.state.windowWidth > 768 ? (
          <DesktopNavigation
            handleLogout={this.handleLogout}
            isLoggedIn={this.state.isLoggedIn}
            cartItemCount={this.state.cartItemCount}
          />
        ) : (
          <MobileNavigation
            setMenuOpen={this.setMenuOpen}
            handleLogout={this.handleLogout}
            isLoggedIn={this.state.isLoggedIn}
            cartItemCount={this.state.cartItemCount}
          />
        )}
      </Responsive>
    );
  }
}

const mapStateToProps = (state) => ({
  cartItemCount: 0,
});

export default connect(mapStateToProps, { addProductsToCart })(Navigation);
