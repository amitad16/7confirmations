import React, { Component } from "react";
import Router from "next/router";
import { Menu, Label, Icon } from "semantic-ui-react";

import styles from "./Navigation.css";

const menu = [
  {
    id: "home",
    title: "Home",
    type: "menu",
    icon: "flaticon-home",
    href: "/",
  },
  {
    id: "products",
    title: "Products",
    type: "menu",
    icon: "flaticon-home",
    href: "/products",
  },
  {
    id: "cart",
    title: "Cart",
    type: "menu",
    icon: "flaticon-home",
    href: "/cart",
  },
];

class MobileNavigation extends Component {
  state = { activeItem: "home", scrollPosition: 0, menuOpen: false };

  componentDidMount() {
    // console.log("Router", Router.pathname);
    this.handleOnUpdate();
    window.addEventListener("scroll", this.handleScroll);

    let activeItem = "home";

    switch (Router.pathname) {
      case "/":
        activeItem = "home";
        break;
      case "/products":
        activeItem = "products";
        break;
      case "/login":
        activeItem = "login";
        break;
      default:
        activeItem = "home";
    }

    this.setState({ activeItem });
  }

  componentDidUpdate() {
    // const isSSR = typeof window === "undefined";
    // if (!isSSR) {
    //   this.setState({ isLoggedIn: !!window.localStorage.getItem("__user__") });
    // }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = (e) => {
    this.setState({ scrollPosition: window.scrollY });
  };

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, menuOpen: false });
  };

  handleOnUpdate = () => {
    this.setState({ windowWidth: window.innerWidth });
  };

  handleToggleMenu = () => {
    this.setState(
      (prevState) => ({ menuOpen: !prevState.menuOpen }),
      () => {
        this.props.setMenuOpen(this.state.menuOpen);
      }
    );
  };

  render() {
    const { activeItem } = this.state;

    // console.log("scroll position = ", this.state.scrollPosition);

    return (
      <>
        <Menu.Item
          name="hamburger"
          active={activeItem === "hamburger"}
          onClick={this.handleToggleMenu}
        >
          <Icon
            name={`${this.state.menuOpen ? "close" : "sidebar"}`}
            // name="sidebar"
            color="black"
            size="large"
            className={styles.MenuOpener}
          />
        </Menu.Item>

        <Menu.Item
          className="logo"
          name="logo"
          // active={activeItem === "logo"}
          onClick={this.handleItemClick}
          href="/"
        >
          7Confirmations
        </Menu.Item>

        <div
          className={styles.MenuSidebar}
          style={{ left: this.state.menuOpen ? "0" : "-100%" }}
        >
          {menu.map((v) => (
            <Menu.Item
              key={v.id}
              className={`mobile ${v.type === "logo" ? "logo" : ""}`}
              name={v.id}
              active={activeItem === v.id}
              onClick={this.handleItemClick}
              href={v.href}
            >
              {v.title}
              {/* {v.id === "cart" && (
                <Label color="teal">{this.props.cartItemCount}</Label>
              )} */}
            </Menu.Item>
          ))}
        </div>

        <Menu.Menu position="right">
          {this.props.isLoggedIn ? (
            <Menu.Item name="logout" onClick={this.props.handleLogout}>
              Logout
            </Menu.Item>
          ) : (
            <Menu.Item
              name="login"
              active={activeItem === "login"}
              onClick={this.handleItemClick}
              href="/login"
            >
              Login
            </Menu.Item>
          )}
        </Menu.Menu>
      </>
    );
  }
}

export default MobileNavigation;
