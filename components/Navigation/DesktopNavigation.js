import React, { Component } from "react";
import Router from "next/router";
import { Menu, Label } from "semantic-ui-react";

const menu = {
  left: [
    {
      id: "logo",
      title: process.env.appName || "7-Confirmations",
      type: "logo",
      icon: "flaticon-home",
      href: "/",
    },
    {
      id: "home",
      title: "Home",
      type: "menu",
      icon: "flaticon-home",
      href: "/",
    },
    {
      id: "products",
      title: "Products",
      type: "menu",
      icon: "flaticon-home",
      href: "/products",
    },
  ],
  right: [
    {
      id: "cart",
      title: "Cart",
      type: "menu",
      icon: "flaticon-home",
      href: "/cart",
    },
    // {
    //   id: "login",
    //   title: "Login",
    //   type: "menu",
    //   icon: "flaticon-home",
    //   href: "/login",
    // },
  ],
};

class DesktopNavigation extends Component {
  state = { activeItem: "home" };

  componentDidMount() {
    let activeItem = "home";

    switch (Router.pathname) {
      case "/":
        activeItem = "home";
        break;
      case "/products":
        activeItem = "products";
        break;
      case "/login":
        activeItem = "login";
        break;
      default:
        activeItem = "home";
    }

    this.setState({ activeItem });
  }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, menuOpen: false });
  };

  render() {
    const { activeItem } = this.state;

    return (
      <>
        {menu.left.map((v) => (
          <Menu.Item
            key={v.id}
            className={`${v.type === "logo" ? "logo" : ""}`}
            name={v.id}
            active={activeItem === v.id}
            onClick={this.handleItemClick}
            href={v.href}
          >
            {v.title}
          </Menu.Item>
        ))}
        <Menu.Menu position="right">
          {menu.right.map((v) => (
            <Menu.Item
              key={v.id}
              className={`${v.type === "logo" ? "logo" : ""}`}
              name={v.id}
              active={activeItem === v.id}
              onClick={this.handleItemClick}
              href={v.href}
            >
              {v.title}
              {/* {v.id === "cart" && (
                <Label color="teal">{this.props.cartItemCount}</Label>
              )} */}
            </Menu.Item>
          ))}

          {this.props.isLoggedIn ? (
            <Menu.Item name="logout" onClick={this.props.handleLogout}>
              Logout
            </Menu.Item>
          ) : (
            <Menu.Item
              name="login"
              active={activeItem === "login"}
              onClick={this.handleItemClick}
              href="/login"
            >
              Login
            </Menu.Item>
          )}
        </Menu.Menu>
      </>
    );
  }
}

export default DesktopNavigation;
