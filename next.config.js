module.exports = {
  env: {
    appName: "7-Confirmations",
  },
};

const withPlugins = require("next-compose-plugins");
const optimizedImages = require("next-optimized-images");

module.exports = withPlugins([
  [optimizedImages, {}],

  // your other plugins here
]);

const withCss = require("@zeit/next-css");
module.exports = withCss({
  cssModules: true,
});
