import Head from "next/head";

// Containers
import LoginPage from "../containers/LoginPage/LoginPage";
import Layout from "../containers/Layout";

function Login() {
  return (
    <>
      <Head>
        <title>7 Confirmations | Login</title>
      </Head>

      <main>
        <Layout>
          <LoginPage />
        </Layout>
      </main>
    </>
  );
}

export default Login;
