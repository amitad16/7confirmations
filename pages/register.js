import Head from "next/head";

// Containers
import RegisterPage from "../containers/RegisterPage/RegisterPage";
import Layout from "../containers/Layout";

function Register() {
  return (
    <>
      <Head>
        <title>7 Confirmations | Register</title>
      </Head>

      <main>
        <Layout>
          <RegisterPage />
        </Layout>
      </main>
    </>
  );
}

export default Register;
