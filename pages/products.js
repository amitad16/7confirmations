import Head from "next/head";

// import { wrapper } from "../store/configureStore";

// Containers
import ProductPage from "../containers/ProductPage/ProductPage";
import Layout from "../containers/Layout";

function Products() {
  return (
    <>
      <Head>
        <title>7 Confirmations | Create Your Personalized Pack</title>
      </Head>

      <main>
        <Layout>
          <ProductPage />
        </Layout>
      </main>
    </>
  );
}

export default Products;
