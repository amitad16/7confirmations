import Head from "next/head";

// Containers
import CartPage from "../containers/CartPage/CartPage";
import Layout from "../containers/Layout";

function Cart() {
  return (
    <>
      <Head>
        <title>7 Confirmations | Cart</title>
      </Head>

      <main>
        <Layout>
          <CartPage />
        </Layout>
      </main>
    </>
  );
}

export default Cart;
