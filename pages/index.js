import Head from "next/head";

// Containers
import HomePage from "../containers/HomePage/HomePage";
import Layout from "../containers/Layout";

function Home() {
  return (
    <>
      <Head>
        <title>7 Confirmations</title>

        <link
          rel="stylesheet"
          type="text/css"
          charSet="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
      </Head>

      <main>
        <Layout>
          <HomePage />
        </Layout>
      </main>
    </>
  );
}

export default Home;
