import App from "next/app";
import { Provider } from "react-redux";
import Router from "next/router";

import * as gtag from "../utils/gtag.util";

import "../public/homepage.css";

import configureStore from "../store/configureStore";

class SevenConfirmations extends App {
  static async getInitialProps({ Component, ctx }) {
    return {
      pageProps: Component.getInitialProps
        ? await Component.getInitialProps(ctx)
        : {},
    };
  }

  componentDidMount() {
    Router.events.on("routeChangeComplete", this.handleRouteChange);
  }

  componentWillUnmount() {
    Router.events.off("routeChangeComplete", this.handleRouteChange);
  }

  handleRouteChange(url) {
    console.log("routeChangeCompleteeeeeeeeeeeeeeeeeeee");

    gtag.pageview(url);

    if (process.env.NODE_ENV !== "production") {
      const els = document.querySelectorAll(
        'link[href*="/_next/static/css/styles.chunk.css"]'
      );
      const timestamp = new Date().valueOf();
      els[0].href = "/_next/static/css/styles.chunk.css?v=" + timestamp;
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Provider store={configureStore()}>
        <Component {...pageProps} />
      </Provider>
    );
  }
}

export default SevenConfirmations;
