import Head from "next/head";

// Containers
import OrderDetails from "../containers/OrderDetails/OrderDetails";
import Layout from "../containers/Layout";

const OrderComplete = (props) => {
  return (
    <>
      <Head>
        <title>7 Confirmations | Order Details</title>
      </Head>

      <main>
        <Layout>
          <OrderDetails
            orderId={props.orderId}
            orderStatus={props.orderStatus}
          />
        </Layout>
      </main>
    </>
  );
};

OrderComplete.getInitialProps = async (ctx) => {
  let props = {
    orderId: ctx.query.orderId,
    orderStatus: ctx.query.orderStatus,
  };

  return props;
};

export default OrderComplete;
