import { combineReducers } from "redux";

// REDUCERS
import appReducer from "../containers/reducers";
import productReducer from "../containers/ProductPage/reducers";
import cartReducer from "../containers/CartPage/reducers";

export default () =>
  combineReducers({
    app: combineReducers({
      auth: appReducer.authReducer,
    }),
    products: productReducer.productReducer,
    cart: cartReducer.cartReducer,
  });
