import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
// import { createLogger } from "redux-logger";
// import { composeWithDevTools } from "redux-devtools-extension";

import rootReducer from "./reducers";

// const logger = createLogger();

const defaultInitialState = {};

export default function configureStore(preloadedState = defaultInitialState) {
  const store = createStore(
    rootReducer(),
    preloadedState,
    applyMiddleware(thunk)
    // composeWithDevTools(applyMiddleware(thunk, logger))
  );

  return store;
}
